/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Sets current date on dateField if it is not already set.
 * Calls makeRequest function.
 * @param {String} dateField Name of the Date field in which to set current date.
 * @param {String} query Query to used for getting data.
 * @param {Array} stringList Arrays of column numbers to align text to left. 
 * @returns {undefined}
 */
function makeRequestDate(dateField, query, stringList)
{
    if (!($(dateField).val()))
    {
        var now = new Date();
        var month = (now.getMonth() + 1);               
        var day = now.getDate();
        if(month < 10) 
            month = "0" + month;
        if(day < 10) 
            day = "0" + day;
        var today = now.getFullYear() + '-' + month + '-' + day;
        $(dateField).val(today);
    }
    makeRequest('1', query, $(dateField).val(), stringList);
}

/**
 * Sets hidden values in form.
 * @returns {Boolean} Returns True in order to submit form and download PDF file.
 */
function print() 
{
    document.getElementById("hidden_data").value = document.getElementById("resultDiv").innerHTML;
    document.getElementById("hidden_title").value = document.title;
    document.getElementById("hidden_css").value = getCSS();
    return true;
}

/**
 * Retrieve all CSSs applied to current page and creates a String to join them 
 * together.
 * @returns {String} Returns all CSSs applied to current page.
 */
function getCSS()
{
    var css = [];
    for (var i=0; i<document.styleSheets.length; i++)
    {
        var sheet = document.styleSheets[i];
        var rules = ('cssRules' in sheet)? sheet.cssRules : sheet.rules;
        if (rules)
        {
            css.push('\n/* Stylesheet : '+(sheet.href||'[inline styles]')+' */');
            for (var j=0; j<rules.length; j++)
            {
                var rule = rules[j];
                if ('cssText' in rule)
                    css.push(rule.cssText);
                else
                    css.push(rule.selectorText+' {\n'+rule.style.cssText+'\n}\n');
            }
        }
    }
    return css.join('\n')+'\n';
}

/***
 * Returns the URL parameter value
 * @param {String} sParam URL parameter
 * @returns {getUrlParameter.sParameterName|Boolean}
 */
function getUrlParameter(sParam)
{
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function ajaxRequestQuery(query, param, queryNumber)
{
    $.ajax({
        type: "POST",
        url: "/RealTime/DBfetch",
        data: { 
            "query": query, 
            "param": param
          },
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',

        //if received a response from the server
        success: function( data, textStatus, jqXHR) {
            $("#localDebugQuery" + queryNumber).show(100);
            $("#localDebugQuery" + queryNumber).html('');
            $("#localDebugQuery" + queryNumber).append(data);
        },

        //If there was no resonse from the server
        error: function(jqXHR, textStatus, errorThrown){
             console.log("Something really bad happened " + textStatus);
              $("#localDebugQuery1").html(jqXHR.responseText);
        }
    });
}

function showQuery(queries, parameter)
{
    var localDebug = getUrlParameter('localDebug');
    if (localDebug === '1')
    {
        for (i = 0; i < queries.length; i++)
        {
            ajaxRequestQuery(queries[i], parameter, i + 1);
        }
    }
    return true;
}

function buildR() 
{
    if ($('#sDate').val() === "" || $('#eDate').val() === "" || $('#count').val() === "")
        return false;
    return $('#sDate').val() + "_" + $('#eDate').val() + "_" + $('#count').val();
}