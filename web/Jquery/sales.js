
function makeSalesRequest() 
{
    query = "";
    param = "";
    if ($('#sname').val())
    {
        query = 'salesSName';
        param = $('#sname').val().toUpperCase();
    }
    else
    {
        if ($('#orgESN').val())
        {
            query = 'salesOrigESN';
            param = $('#orgESN').val();
        }
        else
        {
            if ($('#repESN').val())
            {
                query = 'salesRepESN';
                param = $('#repESN').val();
            }
            else
            {
                if ($('#sBPO').val())
                {
                    query = 'salesBPO';
                    param = $('#sBPO').val();
                }
            }
        }
    }
    if (query !== "")
        makeRequest('1', query, param, [], 'undefined', false, [0, './sales_return_bpo.jsp?sbp=']);
    else
        alert('Please provide at least one search value.');
}

function bpoRequest(bpoValue)
{
    var queries = ['salesCustomerInfo', 'salesShippingInfo', 'salesBillingRefund', 'salesBrightPoint', 'salesCases'];
    var captions = ['Customer Information', 'Shipping Info', 'Billing, Refund & Chargeback Transactions', 'ESN Received at Brightpoint', 'Cases'];
    for (var i = 1; i <= 5; i++)
        makeRequest(String(i), queries[i - 1], bpoValue, [], 'undefined', false, [], captions[i - 1]);
}