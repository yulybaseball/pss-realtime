/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/***
 * Needed because browser doesn't allow to mix content: from HTTPS to HTTP.
 * Get the HTML text specified by URL param.
 * Sets the text in current page, specific div.
 * @param {String} url URL to retrieve the HTTP content from.
 * @param {Boolean} editTable If true, will add the total calls in the last row 
 * of the only table in page. This is specific for availability pages.
 * @returns {Boolean}
 */
function getStaticPage(url, editTable)
{
    $.ajax({
        type: "POST",
        url: "/RealTime/GetStaticPage",
        data: { 
            "url": url
        },
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',

        //if received a response from the server
        success: function( data, textStatus, jqXHR) {
            // apply some CSS to target DIV
            // done here in JS so we don't have to change every page now
            $("#contentDiv1").css("margin", "10px");
            $("#contentDiv1").html(data);
            $(".subtitle").css({'font-weight': 'bold'});
            $(".pagetitle").css({'display':'block', 'font-weight': 'bold', 'font-size': '20px', 'margin': '10px', 'color': '#0099d1'});
            $(".titlefont").css({'font-weight': 'bold', 'background-color': '#0099d1', 'color': 'white'});
            
            if (editTable)
            {
                $('#contentDiv1').contents()
                    .filter(function() { return this.nodeType === 3; })
                    .remove();
                setTotalCalls();
                $('table tr:first, tr:last').css({'font-weight': 'bold'});
            }
        },

        //If there was no response from the server
        error: function(jqXHR, textStatus, errorThrown){
            console.log("Something really bad happened: " + textStatus);
            $("#contentDiv1").html(jqXHR.responseText);
        }
    });
    return true;
}

/***
 * Loops through the only table in page and sums the number in the 
 * 5th cell of every row.
 * When reaches the final row, set the Total text alongside the total calls.
 * @returns {undefined}
 */
function setTotalCalls()
{
    var total = 0;
    var rowCount = $('table tr').length;
    $('table').find('tr').each(function (rowIndex, r) {
        if (rowIndex !== 0)
        {
            $(this).find('td').each(function (colIndex, c) {
                // this is the colIndex where it is the call value
                if (colIndex === 5)
                {
                    if (rowIndex === (rowCount - 1)) //last row
                        $(this).text("Total: " + total);
                    else
                        total += parseInt($(this).html());
                }
            });
        }
    });
}