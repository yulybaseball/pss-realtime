/* 
 * Created by Raul Tobo
 */

function loadHome(div) {
    $('#hmDiv' + div).contents().find("head").append($("<style type='text/css'>  .pBody{min-width:1px}  </style>"));
}

function goHome()
{
    location.reload();
}

function expand(obj) {
    var target = obj.parentNode;
    target.className = (target.className === "menu-item active" ? "menu-item" : "menu-item active");
}
function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}

/***
 * Make the request to server using Ajax.
 * @param {String} nQueries Number that identifies the order of the DIV that will receive the data.
 * @param {String} qSelection Name of the SQL query in DBFetch.java
 * @param {String} qParameter Value of the parameter to execute the SQL query
 * @param {Array} colStringList Columns numbers whose text needs to be aligned 
 * to the left.
 * @param {String} db Database name. RTRP by default.
 * @param {type} ota_summary
 * @param {Array} link_options
 * @param {String} tableCaption
 * @returns {undefined}
 */
function makeRequest(nQueries, qSelection, qParameter, colStringList, db, ota_summary, link_options, tableCaption) 
{
    $('#contentDiv' + nQueries).empty();
    $('#load').css('display', 'inline-block');
    var xmlHttpRequest = getXMLHttpRequest();
    xmlHttpRequest.open("GET", "/RealTime/DBfetch?qSelection=" + qSelection + "&qParameter=" + qParameter + "&qDB=" + db, true);
    xmlHttpRequest.onreadystatechange = function () 
    {
	getReadyStateHandler(xmlHttpRequest, nQueries, colStringList, ota_summary, link_options, tableCaption);
    };
    xmlHttpRequest.send();
}

function getXMLHttpRequest() {
    var xmlHttpReq;
    if (window.XMLHttpRequest) {
	xmlHttpReq = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
	try {
	    xmlHttpReq = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (exp1) {
	    try {
		xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch (exp2) {
		alert("exception in getXMLHttpRequest()!");
	    }
	}
    }
    return xmlHttpReq;
}

/**
 * Gets the result of executing the query in database (as string),
 * and creates the table to show that information.
 * @param {type} xmlHttpRequest Request from the server
 * @param {type} nQueries
 * @param {Array} colStringList Number of columns whose text needs to be aligned 
 * to the left.
 * @param {Boolean} ota_summary If True, table to be created for final results
 * will have different columns structure (see function createOtaSummaryCols()).
 * @param {Array} link_options Options to create the link in specific column value. Array: [columnNumber, uri]
 * @returns {Undefined}
 */
function getReadyStateHandler(xmlHttpRequest, nQueries, colStringList, ota_summary, link_options, tableCaption) 
{
    if (xmlHttpRequest.readyState == 4) 
    {
	if (xmlHttpRequest.status == 200) 
        {
	    $('#load').css('display', 'none');
	    var result = xmlHttpRequest.responseText;
	    var tbId = "resultTb" + nQueries;
            var tc = tableCaption ? ("<caption>" + tableCaption + "</caption>") : "";
	    var table = $('<table>' + tc + '<thead></thead><tbody></tbody></table>').attr('id', tbId);
	    $('#contentDiv' + nQueries).append(table);
	    var rows = result.split("\^");
	    var columns;
	    var rowToInsert = "";
            // if this table is for ota_summary
            // we need to check for the second, fifth, eigth and eleventh 
            // columns, and add a new row before them
            // ONLY for first row
            if (ota_summary)
                rowToInsert = createOtaSummaryCols();

	    for (var i = 0; i < rows.length; i++) 
            {
		if (rows[i] != "") 
                {
		    rowToInsert = (i === 0 && ota_summary) ? rowToInsert + "<tr>" : "<tr>";
		    columns = rows[i].split("*");
		    for (var j = 0; j < columns.length; j++) 
                    {
                        columnStr = "<td>";
                        // if @colStringList is comming with some value
                        // we need to set that column text-align: left
                        // it assumes columns count starts at 1
                        if (colStringList && colStringList.indexOf(j + 1) !== -1 && i !== 0)
                            columnStr = "<td style=\"text-align: left\">";
                        var cellValue = replaceColumnNames(columns[j]);
                        // check if the value for the cell needs to be enclosed in a link
                        if (link_options && link_options[0] === j && i !== 0)
                            cellValue = setURI(cellValue, link_options);
			rowToInsert = rowToInsert + columnStr + cellValue + "</td>";
		    }
		    rowToInsert = rowToInsert + "</tr>";
		    if (i == 0) 
                    {
			$('#' + tbId + ' > thead:last').append(rowToInsert);
		    } 
                    else 
                    {
			$('#' + tbId + ' > tbody:last').append(rowToInsert);
		    }
		}
	    }
	} 
        else 
        {
	    $('#load').css('display', 'none');
	    $('#contentDiv' + nQueries).append(xmlHttpRequest.responseText);
	}
    }
}

function setURI(cellValue, link_options)
{
    return '<a target="_blank" href="' + link_options[1] + cellValue + '">' + cellValue + '</a>';
}

function createOtaSummaryCols()
{
    return "<tr><td></td><td colspan='3'>REDEMPTION</td>" + 
                        "<td colspan='3'>ACTIVATION</td>" + 
                        "<td colspan='3'>REACTIVATION</td>" + 
                        "<td colspan='3'>PERSONALITY</td>" + 
            "</tr>";
}

/**
 * Add single quotes to every item in a comma-separated string 
 * comming in <strong>qParameter</strong>.
 * Rest of parameters are only used to call <strong>makeRequest()</strong> function.
 * @param {type} nQueries
 * @param {type} qSelection
 * @param {type} qParameter
 * @param {type} colStringList
 * @returns {undefined}
 */
function fixParameterList(nQueries, qSelection, qParameter, colStringList)
{
    qParameter = qParameter.replace(/,/g, '\',\'');
    qParameter = '\'' + qParameter + '\'';
    makeRequest(nQueries, qSelection, qParameter, colStringList);
}

function replaceColumnNames(text)
{
    var inValue = text.replace("CURRENT_HOUR", "TIME");
    inValue = inValue.replace("T_HOUR", "TIME");
    inValue = inValue.replace("T_DATE", "DATE");
    inValue = inValue.replace("X_SOURCESYSTEM", "SOURCE");
    inValue = inValue.replace("X_ACTION_TEXT", "ACTION");
    inValue = inValue.replace("X_ACTION_TYPE", "ACTION TYPE");
    inValue = inValue.replace("X_SUB_SOURCESYSTEM", "BRAND");
    inValue = inValue.replace("SUB_SOURCESYSTEM", "BRAND");
    inValue = inValue.replace("SUCCESS_RATE", "SUCCESS RATE (%)");
    inValue = inValue.replace("STATUS_MESSAGE", "MESSAGE");
    inValue = inValue.replace("DATE_OF_TRANS", "DATE");
    inValue = inValue.replace("TOTAL_TRANS", "TOTAL");
    inValue = inValue.replace("ORDER_TYPE", "ORDER TYPE");
    inValue = inValue.replace("ACTION_ITEM_ID", "AID");
    inValue = inValue.replace("UPDATE_DATE", "UPDATED");
    inValue = inValue.replace("CREATION_DATE", "CREATED");
    inValue = inValue.replace("Q_TRANSACTION", "Q TRANSACTION");
    inValue = inValue.replace("X_DEPOSIT", "AMOUNT");
    inValue = inValue.replace("X_REQ_DATE_TIME", "REQ TIME");
    inValue = inValue.replace("X_STATUS", "STATUS");
    inValue = inValue.replace("X_MIN", "MIN");
    inValue = inValue.replace("X_ESN", "ESN");
    inValue = inValue.replace("X_MODE", "MODE");
    inValue = inValue.replace("X_REASON", "REASON");
    inValue = inValue.replace("X_CARRIER_CODE", "CARRIER");
    inValue = inValue.replace("TOTALPASSED", "Total Passed");
    inValue = inValue.replace("TOTALATTEMPT", "Total Attempts");
    inValue = inValue.replace("PERCENT_SUCCESS", "Success (%)");
    inValue = inValue.replace("PERCENT_TOTAL", "Total (%)");
    inValue = inValue.replace("SOURCESYSTEM", "SOURCE SYSTEM");
    inValue = inValue.replace("ERROR_MESSAGE", "MESSAGE");
    inValue = inValue.replace("X_RQST_SOURCE", "REQUEST SOURCE");
    inValue = inValue.replace("X_RQST_TYPE", "REQUEST TYPE");
    inValue = inValue.replace("X_MERCHANT_ID", "MERCHANT");
    inValue = inValue.replace("X_ICS_RCODE", "ICS CODE");
    inValue = inValue.replace("X_ICS_RFLAG", "ICS FLAG");
    inValue = inValue.replace("X_ICS_RMSG", "ICS MESSAGE");
    inValue = inValue.replace("X_CUSTOMER_EMAIL", "CUSTOMER EMAIL");
    inValue = inValue.replace("CASE_TYPE", "CASE TYPE");
    inValue = inValue.replace("TOTAL_OPEN", "TOTAL OPEN");
    inValue = inValue.replace("MY_DATE", "DATE");
    inValue = inValue.replace("TRANSACTION_TYPE", "TRANSACTION TYPE");
    inValue = inValue.replace("CASE_ID", "CASE ID");
    inValue = inValue.replace("TNUM", "COUNT");
    inValue = inValue.replace("TOTALCOUNT", "COUNT");
    inValue = inValue.replace("MYCOUNT", "COUNT");
    inValue = inValue.replace("CARD_STATUS", "CARD STATUS");
    inValue = inValue.replace("X_TRANSACDATE", "TRANSACTION DATE");
    inValue = inValue.replace("X_ILD_TRANS_TYPE", "ILD TRANSACTION TYPE");
    inValue = inValue.replace("X_ILD_STATUS", "ILD STATUS");
    inValue = inValue.replace("X_LAST_UPDATE", "LAST UPDATE");
    inValue = inValue.replace("X_PRODUCT_ID", "PRODUCT ID");
    inValue = inValue.replace("X_API_STATUS", "API STATUS");
    inValue = inValue.replace("X_API_MESSAGE", "API MESSAGE");
    inValue = inValue.replace("UNLOCK_STATUS", "UNLOCK STATUS");
    inValue = inValue.replace("SUBSCR_MDN", "SUBSCR MIN");
    inValue = inValue.replace("MSG_TIMESTAMP", "MSG DATETIME");
    inValue = inValue.replace("MESSG_DIR", "MSG DIR");
    inValue = inValue.replace("PARSED_MSG", "PARSED MSG");
    inValue = inValue.replace("ORIGINAL_MSG", "ORIGINAL MSG");
    inValue = inValue.replace("PIN_NUMBER", "PIN #");
    inValue = inValue.replace("SOURCE_SYSTEM", "SRC SYSTEM");
    return inValue;
}