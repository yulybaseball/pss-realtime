<%-- 
    Document   : error
    Created on : Aug 2, 2017, 2:53:43 PM
    Author     : rtobo
--%>

<%@page isErrorPage="true" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <link rel="shortcut icon" href="/RealTime/images/tracIcon.png" />
        <title>Ooops!! Error Encountered</title>
    </head>
    <body style="padding:25px;">
	<center>
            <h1 id="errorPageHeader">Ooops... Something Went Wrong...</h1>
	    <image src="/RealTime/images/broken_robot.jpg" alt="T_T" width="300px" height="283px" />
            <p>Please see error below. If problem persists, please contact PSS.</p>
            <div id="errorPageErrorExp">
                <b>Response Code:</b> ${pageContext.errorData.statusCode}<br />
                <b>Message:</b> ${pageContext.errorData.throwable}
            </div>
	</center>
    </body>
</html>
