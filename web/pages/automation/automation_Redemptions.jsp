<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Automation - Redemptions</title>

        <script type="text/javascript">
            $(document).ready( function()
            {
                makeRequestDate('#bday', 'automationRedemptions');
                showQuery(Array.from(new Set(["automationRedemptions"])), $('#bday').val());
            });
        </script>
    </head>
    <BODY class="pBody">
        
        <div id="localDebugQuery1"></div>
        
	<div id="top">
	    <center>
		<form action="/RealTime/Print" method="post" target="_blank" id="print_form" onsubmit="javascript: return print();" >
                    <input type="hidden" id="hidden_data" name="data" />
                    <input type="hidden" id="hidden_title" name="title" />
                    <input type="hidden" id="hidden_css" name="css" />
                    <h3>
                        <input type="submit" value="PDF" class="btn" id="print_button" title="Download result as PDF" />
                        Automation - Redemptions
                    </h3>
                </form>
		<button class="btn" onClick="makeRequest('1', 'automationRedemptions', $('#bday').val());showQuery(Array.from(new Set(['automationRedemptions'])), $('#bday').val());">Refresh</button><br/><a id="load"></a>
                <br />
                <div style="margin-bottom: 10px">
                    Date: <input type="date" name="bday" id="bday" max="2050-12-31" min="2000-01-01" />
                    <h6>Select a date and click Refresh button to show All Redemptions on selected date.</h6>
                </div>
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" style="min-height:250px;">
	    <div id="contentDiv1">
	    </div>
	</div>
    </BODY>

</html>
