<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>OTA - MIN Lookup (OTAPRD)</title>
        
        <script>$(document).ready(showQuery(Array.from(new Set(["OTACheckMINOTAPRD"])), '<%=request.getParameter("MIN")%>'));</script>
        
    </head>
    <BODY onLoad="makeRequest('1', 'OTACheckMINOTAPRD', '<%=request.getParameter("MIN")%>', [], 'ota')" class="pBody">
        
        <div id="localDebugQuery1"></div>
        
	<div id="top">
	    <center>
                <h3>
                    OTA - MIN Lookup (OTAPRD)
                </h3>
                <br/>
                <button class="btn" onClick="makeRequest('1', 'OTACheckMINOTAPRD', '<%=request.getParameter("MIN")%>', [], 'ota')">Refresh</button><br/><br/><a id="load"></a>
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" style="min-height:250px;">
	    <div id="contentDiv1">
	    </div>
	</div>
    </BODY>

</html>
