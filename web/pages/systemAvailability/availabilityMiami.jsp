<%-- 
    Document   : monitorM
    Created on : Nov 7, 2017, 11:13:52 AM
    Author     : mabdel-kader
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/http-domains.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>IVR Availability - Miami</title>
        
        <script type="text/javascript" language="javascsript">
            $(function() 
            {
                getStaticPage("http://dpivrweb/monitorM.html", true);
            });
        </script>
        
    </head>
    <BODY class="pBody">
	<div id="top">
            <center>
		<h3>IVR Availability - Miami</h3>
		<br/>
		<button class="btn" onClick="location.href='/RealTime/pages/systemAvailability/availabilityMiami.jsp'">Refresh</button><br/><a id="load"></a>
	    </center>
	</div>
	<div id="textDiv">
	</div>
	<div style="height:95%" >
	    <div id="contentDiv1"></div>
	</div>
    </BODY>

</html>
