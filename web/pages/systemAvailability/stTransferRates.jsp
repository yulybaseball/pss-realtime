<%-- 
    Document   : monitorM
    Created on : Nov 7, 2017, 11:13:52 AM
    Author     : mabdel-kader
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Straight Talk IVR Transfer Rates</title>
        
        
    </head>
    <BODY class="pBody">
	<div id="top">
	    <center>
		<h3>Straight Talk IVR Transfer Rates</h3>
		<br/>
		<!--<button class="btn" onClick="makeRequest('1', 'activationsByBrand', $('#bday').val(), [2])">Refresh</button><br/><a id="load"></a>-->
                <br/>
	    </center>
	</div>
	<div id="textDiv">
	</div>
	<div id="resultDiv" style="min-height:250px;">
	    <div id="contentDiv1" style="height:100%">
                <object type="text/html" style="height:95%;width:100%"data="http://dpivrweb/st_xfer.html"></object>
	    </div>
	</div>
    </BODY>

</html>
