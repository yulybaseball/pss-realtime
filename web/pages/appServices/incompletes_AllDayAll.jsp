<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Incompletes All Day</title>
        
        <script>$(document).ready(showQuery(Array.from(new Set(["incompletesAllDayBP"]))));</script>
        
    </head>
    <BODY onLoad="makeRequest('1', 'incompletesAllDayBP');makeRequest('2', 'incompletesAllDayApp')" class="pBody">
        
        <div id="localDebugQuery1"></div>
        
	<div id="top">
	    <center>
		<br/>
		<button class="btn" onClick="makeRequest('1', 'incompletesAllDayBP');makeRequest('2', 'incompletesAllDayApp')">Refresh</button><br/><a id="load"></a>
		<br/>
		<h3>Incompletes All Day - BP</h3>
		<h5>(x_program_purch_hdr)</h5>
		<br/>		
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" >
	    <div id="contentDiv1">
	    </div>
	</div>
	<div id="top">
	    <center>
		<br/>
		<h3>Incompletes All Day - APP</h3>
		<h5>(table_x_purch_hdr)</h5>
		<br/>
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" >
	    <div id="contentDiv2">
	    </div>
	</div>
    </BODY>

</html>
