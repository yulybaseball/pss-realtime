<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Tracfone-Safelink - Transfer Rates</title>
        
        <script>$(document).ready(showQuery(Array.from(new Set(["activationsReactivationsTransferRates", "refillTransferRates"]))));</script>
        
    </head>
    <BODY onLoad="makeRequest('1', 'activationsReactivationsTransferRates', null, [], 'pwprod');makeRequest('2', 'refillTransferRates', null, [], 'pwprod')" class="pBody">
	
        <div id="localDebugQuery1"></div>
        <div id="localDebugQuery2"></div>
        
        <div id="top">
	    <center>
		<br/>
		<button class="btn" onClick="makeRequest('1', 'activationsReactivationsTransferRates', null, [], 'pwprod');makeRequest('2', 'refillTransferRates', null, [], 'pwprod')">Refresh</button><br/><a id="load"></a>
		<br/>
		<h3>Activations/Reactivations Transfer Rates</h3>
		<br/>		
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" >
	    <div id="contentDiv1">
	    </div>
	</div>
	<div id="top">
	    <center>
		<br/>
		<h3>Refill Transfer Rates</h3>
		<br/>
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" >
	    <div id="contentDiv2">
	    </div>
	</div>
    </BODY>

</html>
