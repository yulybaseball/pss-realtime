<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Low Balance Params</title>
        
        <script>$(document).ready(showQuery(Array.from(new Set(["LowBalanceParams"]))));</script>
        
    </head>
    <BODY onLoad="makeRequest('1', 'LowBalanceParams', null, [3, 5])" class="pBody">
        
        <div id="localDebugQuery1"></div>
        
	<div id="top">
	    <center>
		<form action="/RealTime/Print" method="post" target="_blank" id="print_form" onsubmit="javascript: return print();" >
                    <input type="hidden" id="hidden_data" name="data" />
                    <input type="hidden" id="hidden_title" name="title" />
                    <input type="hidden" id="hidden_css" name="css" />
                    <h3>
                        <input type="submit" value="PDF" class="btn" id="print_button" title="Download result as PDF" />
                        Low Balance Params
                    </h3>
                </form>
		<button class="btn" onClick="makeRequest('1', 'LowBalanceParams', null, [3, 5])">Refresh</button><br/><br/><a id="load"></a>
	
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" style="min-height:250px;">
	    <div id="contentDiv1">
	    </div>
	</div>
    </BODY>

</html>
