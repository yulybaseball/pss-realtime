<%-- 
    Document   : cybersource_5min
    Created on : Jul 20, 2017, 4:21:33 PM
    Author     : rtobo
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
        <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
        <script src="/RealTime/Jquery/rtobo.js"></script>
        <script src="/RealTime/Jquery/util.js"></script>
        <link rel="shortcut icon" href="../images/tracIcon.png" />	
        <title>Warehouse Recurring Cases</title>
        
        <script>
            function validParamsRequest() 
            {
                var params = buildR();
                if (!params)
                    alert("Should specify both dates and Count.");
                else
                {
                    makeRequest('1', 'warehouseRecurring', params);
                    showQuery(Array.from(new Set(['warehouseRecurring'])), params);
                }
            }
        </script>
    </head>

    <BODY class="pBody">
        
        <div id="localDebugQuery1"></div>

	<div id="top">
            <center>
		<form action="/RealTime/Print" method="post" target="_blank" id="print_form" onsubmit="javascript: return print();" >
                    <input type="hidden" id="hidden_data" name="data" />
                    <input type="hidden" id="hidden_title" name="title" />
                    <input type="hidden" id="hidden_css" name="css" />
                    <h3>
                        <input type="submit" value="PDF" class="btn" id="print_button" title="Download result as PDF" />
                        Warehouse Recurring Cases
                    </h3>
                </form>
                <button class="btn" onClick="validParamsRequest()">Refresh</button><br/><a id="load"></a>
                <br />
                <div style="margin-bottom: 10px">
                    <input type="date" name="sDate" id="sDate" max="2050-12-31" min="2000-01-01" placeholder="Beginning Date" />
                    <input type="date" name="eDate" id="eDate" max="2050-12-31" min="2000-01-01" placeholder="Ending Date" />
                    <input type="number" name="count" id="count" max="2050-12-31" min="2000-01-01" placeholder="Count (greater than 2)" />
                    <h6>Select Beginning & Ending dates, and Count; click Refresh button to show Warehouse Recurring Cases.</h6>
                </div>
	    </center>
	</div>
	<div id="textDiv">	    
	</div>
	<div id="resultDiv" style="min-height:250px;">
	    <div id="contentDiv1">
	    </div>
	</div>
    </BODY>

</html>
