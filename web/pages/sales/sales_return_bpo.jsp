<html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
            <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
            <script src="/RealTime/Jquery/rtobo.js"></script>
            <script src="/RealTime/Jquery/sales.js"></script>
            <link rel="shortcut icon" href="../../images/tracIcon.png" />

            <title>Sales Return V.3</title>
        </head>
        <body onload="bpoRequest('<%=request.getParameter("sbp")%>')">
            <div style="text-align: center">
                <h3 style="font-size: large">Sales Return V.3</h3>
            </div>
            <div id="resultDivSales">BP Order: <b><%=request.getParameter("sbp")%></b>
                <center><a id="load"></a></center>
                <div id="contentDiv1" style="margin-top: 25px;">
                </div>
                <div id="contentDiv2" style="margin-top: 25px;">
                </div>
                <div id="contentDiv3" style="margin-top: 25px;">
                </div>
                <div id="contentDiv4" style="margin-top: 25px;">
                </div>
                <div id="contentDiv5" style="margin-top: 25px;">
                </div>
            </div>

        </body>
</html>
