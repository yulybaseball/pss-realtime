<html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/RealTime/style.css" rel="stylesheet" type="text/css"/>
            <script src="/RealTime/Jquery/jquery-1.11.3.js"></script>
            <script src="/RealTime/Jquery/rtobo.js"></script>
            <script src="/RealTime/Jquery/sales.js"></script>
            <link rel="shortcut icon" href="../../images/tracIcon.png" />

            <title>Sales Return V.3</title>
        </head>
        <body>
            
            <div style="text-align: center">
                <h3 style="font-size: large">Sales Return V.3</h3>
            </div>
            <table>
                <caption>Search Filter</caption>
                <tr>
                    <td style="text-align: right">Customer Full Name:</td>
                    <td><input type="text" id="sname" size="50"></td>
                </tr>
                <tr>
                    <td style="text-align: right">Original ESN:</td>
                    <td><input type="text" id="orgESN" size="50"></td>
                </tr>
                <tr>
                    <td style="text-align: right">Replacement ESN:</td>
                    <td><input type="text" id="repESN" size="50"></td>
                </tr>
                <tr>
                    <td style="text-align: right">BP Order Number:</td>
                    <td><input type="text" id="sBPO" size="50"></td>
                </tr>
            </table>
            <center>
                <button class="btn" onClick="makeSalesRequest()">Search</button><br/><br/><a id="load"></a>
            </center>
            

            <div id="resultDiv" style="min-height:250px;">
                <div id="contentDiv1">
                </div>
            </div>
            
        </body>
</html>
