package controllers;

/*
 * DB Class
 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * 
 */
@WebServlet(name = "GetStaticPage", urlPatterns =
{
    "/GetStaticPage"
})
public class StaticPageFetcher extends HttpServlet
{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) 
                throws ServletException, IOException 
    {
        URL u = null;
        InputStream is = null;
        DataInputStream dis;
        String s;
        String allString = "";
        PrintWriter out = resp.getWriter();

        try 
        {
           u = new URL(req.getParameter("url"));
           is = u.openStream();         // throws an IOException
           dis = new DataInputStream(new BufferedInputStream(is));
           while ((s = dis.readLine()) != null) 
           {
              allString += s;
           }
        } 
        catch (MalformedURLException mue) 
        {
           System.out.println("Ouch - a MalformedURLException happened.");
        } 
        catch (IOException ioe) 
        {
           System.out.println("Oops- an IOException happened.");
           throw new IOException("Connection couldn't be established to " + u);
        } 
        finally 
        {
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException ioe) 
                {
                   // just going to ignore this one
                }
            }
        } // end of 'finally' clause

        try
        {
            out.write(allString);
        }
        catch (Exception e)
        {
            out.println(e.getMessage());
        }
        finally
        {
            out.flush();
            out.close();
        }
    }
}
