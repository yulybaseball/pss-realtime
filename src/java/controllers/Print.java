package controllers;

/*
 * DB Class
 */

import com.itextpdf.text.BaseColor;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletContext;

/**
 *
 * @author rtobo
 */
@WebServlet(name = "Print", urlPatterns =
{
    "/Print"
})
public class Print extends HttpServlet
{
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) 
                throws ServletException, IOException
    {
        String title = req.getParameter("title");
        resp.setContentType("application/pdf");
        resp.setHeader("Content-Disposition", "attachment;filename=\"" + title + "\".pdf");
        Document document = new Document();
        try
        {
            document.setPageSize(PageSize.LETTER);
            PdfWriter writer = PdfWriter.getInstance(document, resp.getOutputStream());
            document.open();
            ServletContext context = getServletContext(); // Inherited from HttpServlet.
            Image img = Image.getInstance(context.getResource("/images/logos/tf_logo.png").getPath());
            img.scalePercent(25);
            PdfPTable header = new PdfPTable(2);
            header.setWidths(new int[]{2, 10});
            header.setTotalWidth(PageSize.LETTER.getWidth() - 35);
            header.setLockedWidth(true);
            header.getDefaultCell().setFixedHeight(2);
            header.getDefaultCell().setBorder(Rectangle.BOTTOM);
            header.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
            header.addCell(img);
            PdfPCell text = new PdfPCell();
            text.setPaddingBottom(8);
            text.setPaddingLeft(10);
            text.setBorder(Rectangle.BOTTOM);
            text.setBorderColor(BaseColor.LIGHT_GRAY);
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            text.addElement(new Phrase(title + " (Updated: " + dateFormat.format(new Date()) + ")", new Font(Font.FontFamily.HELVETICA, 12)));
            header.addCell(text);
            header.writeSelectedRows(0, -1, 15, PageSize.LETTER.getHeight() - 5, writer.getDirectContent());
            
            ByteArrayInputStream bis = new ByteArrayInputStream(req.getParameter("data").getBytes());
            ByteArrayInputStream cis = new ByteArrayInputStream(req.getParameter("css").getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, bis, cis);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            document.close();
        }
    }
}
