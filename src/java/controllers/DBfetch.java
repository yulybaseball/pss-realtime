package controllers;

/*
 * DB Class
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.routines.EmailValidator;

/**
 *
 * @author rtobo
 */
@WebServlet(name = "DBfetch", urlPatterns =
{
    "/DBfetch"
})
public class DBfetch extends HttpServlet
{
    /**
     * This method is called to create the connection to the database. if the
     * connection is successful then it returns the db connection. If not then
     * it throws an exception. The DB information is contained in this method.
     * URL/USER/PASS
     *
     * @return Connection conn: returns the successful db connection.
     */
    private static Connection dbConnect(String db) throws SQLException
    {
        Connection conn = null;
        try
        {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } 
        catch (ClassNotFoundException e)
        {
            System.out.println("Can't find JDBC Driver");
        }
        try
        {
            Map<String, List<String>> mapOfList = new HashMap<>();
            mapOfList.put("ota", Arrays.asList("web_report", "otareport", "jdbc:oracle:thin:@otadb:1545:otaprd"));
            mapOfList.put("rtrp", Arrays.asList("web_report", "RealTimeWebRpt", "jdbc:oracle:thin:@dpe1087008:1531:CLFYRTRP"));
            mapOfList.put("pwprod", Arrays.asList("PWAS_ADM", "abd123", "dbc:oracle:thin:@dpvmbiprd:1521:dwprod"));

            conn = DriverManager.getConnection(mapOfList.get(db).get(2), 
                                                mapOfList.get(db).get(0), 
                                                mapOfList.get(db).get(1));
        } 
        catch (SQLException e)
        {
            System.out.println("Could not connect to DB");
        }
        return conn;
    }
    
    private static Connection dbConnect() throws SQLException
    {
        return dbConnect("rtrp");
    }

    /**
     * This method will receive 1 or 2 parameters. The first (mandatory) is the
     * name of the query to be executed. The second (optional) can be used to
     * insert user specific info into the query. The method will return a string
     * containing the query. If the input is incorrect then an
     * IllegalArgumentException is thrown
     *
     * @param qSelection Mandatory parameter that the method will use to
     * identify the correct query to execute.
     * @param qParameter Optional parameter that can be used to insert user
     * specific info into the query. Such a date, ESN, etc.
     *
     * @return String theQuery: Method returns a string containing the selected
     * query.
     */
    private static String getQuery(String qSelection, String qParameter)
    {
        String theQuery;
        switch (qSelection)
        {
            //QUERIES FOR APP SERVICES
            case "commerce1min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT(*) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL FROM x_biz_purch_hdr WHERE X_RQST_DATE >= (sysdate-1/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "commerce5min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT( *) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL FROM x_biz_purch_hdr WHERE X_RQST_DATE >= (sysdate-5/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "commerce20min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT( *) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL FROM x_biz_purch_hdr WHERE X_RQST_DATE >= (sysdate-20/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "commerceCurrentHr":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, sum(decode(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT(*) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/count(*),2) *100 PERCENT_SUCCESS, round(sum(decode(x_ics_rcode,1,1,100,1,0))/sum(count(*)) over(),2) *100 PERCENT_TOTAL from x_biz_purch_hdr where X_RQST_DATE >= (sysdate-1/24) and x_rqst_date <= (sysdate)and x_rqst_source is not null group by x_rqst_source, x_merchant_id";
                break;
            case "commerceAllDay":
                theQuery = "Select x_rqst_source SOURCE, x_merchant_id COMPANY, sum(decode(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, count(*) TOTALATTEMPT, round(sum(decode(x_ics_rcode,1,1,100,1,0))/count(*),2) *100 PERCENT_SUCCESS, round(sum(decode(x_ics_rcode,1,1,100,1,0))/sum(count(*)) over(),2) *100 PERCENT_TOTAL from x_biz_purch_hdr where X_RQST_DATE >= trunc(sysdate) and X_RQST_DATE <= trunc(sysdate) + 1 and x_rqst_source is not null group by x_rqst_source, x_merchant_id";
                break;
            case "cyber1min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT(*) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL FROM table_x_purch_hdr WHERE X_RQST_DATE >= (sysdate-1/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "cyber5min":
                theQuery = "SELECT x_rqst_source SOURCE,x_merchant_id COMPANY,SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED,COUNT(*) TOTALATTEMPT,ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS,ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL  FROM table_x_purch_hdr  WHERE X_RQST_DATE >= (sysdate-5/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL  GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "cyber20min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, COUNT( *) TOTALATTEMPT, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 PERCENT_SUCCESS, ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 PERCENT_TOTAL FROM table_x_purch_hdr WHERE X_RQST_DATE >= (sysdate-20/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "cyberCurrentHr":
                theQuery = "Select x_rqst_source SOURCE, x_merchant_id COMPANY, sum(decode(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, count(*) TOTALATTEMPT, round(sum(decode(x_ics_rcode,1,1,100,1,0))/count(*),2) *100 PERCENT_SUCCESS, round(sum(decode(x_ics_rcode,1,1,100,1,0))/sum(count(*)) over(),2) *100 PERCENT_TOTAL from table_x_purch_hdr where X_RQST_DATE >= (sysdate-1/24) and x_rqst_date <= (sysdate)and x_rqst_source is not null group by x_rqst_source, x_merchant_id";
                break;
            case "cyberAllDay":
                theQuery = "Select x_rqst_source SOURCE, x_merchant_id COMPANY, sum(decode(x_ics_rcode,1,1,100,1,0)) TOTALPASSED, count(*) TOTALATTEMPT, round(sum(decode(x_ics_rcode,1,1,100,1,0))/count(*),2) *100 PERCENT_SUCCESS, round(sum(decode(x_ics_rcode,1,1,100,1,0))/sum(count(*)) over(),2) *100 PERCENT_TOTAL from table_x_purch_hdr where X_RQST_DATE >= trunc(sysdate) and X_RQST_DATE <= trunc(sysdate) + 1 and x_rqst_source is not null group by x_rqst_source, x_merchant_id";
                break;
            case "cyberFailCurrentHr":
                theQuery = "select x_rqst_source SOURCESYSTEM,x_merchant_id COMPANY,x_ics_rmsg ERROR_MESSAGE,count(*) COUNT from table_x_purch_hdr where x_ics_rcode<>1 and X_RQST_DATE >= (sysdate-1/24) and X_RQST_DATE <= (sysdate) group by x_rqst_source,x_merchant_id,x_ics_rmsg order by x_rqst_source, count(*) desc";
                break;
            case "cyberFailAllDayChannel":
                theQuery = "select x_rqst_source SOURCESYSTEM,x_ics_rmsg ERROR_MESSAGE,count(*) COUNT from table_x_purch_hdr where x_ics_rcode<>1 and X_RQST_DATE >= trunc(sysdate)-1 group by x_rqst_source,x_ics_rmsg order by x_rqst_source, count(*) desc";
                break;
            case "cyberFailAllDay":
                theQuery = "select x_ics_rmsg ERROR_MESSAGE,count(*) COUNT from table_x_purch_hdr where x_ics_rcode<>1 and X_RQST_DATE >= trunc(sysdate)-1 group by x_ics_rmsg order by count(*) desc";
                break;
            case "incompletesAllDayBP":
                theQuery = "SELECT TO_CHAR (X_RQST_DATE, 'HH24:MI') \"TIME\", COUNT (*) \"COUNT\" FROM x_program_purch_hdr WHERE x_rqst_date >= trunc(SYSDATE) AND ( x_ics_rflag IN ('ESYSTEM','ETIMEOUT','TIMEOUT','INCOMPLETE') OR x_ics_rflag IS NULL) AND x_merchant_id IS NOT NULL GROUP BY TO_CHAR (X_RQST_DATE, 'HH24:MI') ORDER BY TO_CHAR (X_RQST_DATE, 'HH24:MI') DESC";
                break;
            case "incompletesAllDayApp":
                theQuery = "SELECT TO_CHAR (X_RQST_DATE, 'HH24:MI') \"TIME\", COUNT (*) \"COUNT\" FROM table_x_purch_hdr WHERE x_rqst_date >= trunc(SYSDATE) AND ( x_ics_rflag IN ('ESYSTEM','ETIMEOUT','TIMEOUT','INCOMPLETE') OR x_ics_rflag IS NULL) AND x_merchant_id IS NOT NULL GROUP BY TO_CHAR (X_RQST_DATE, 'HH24:MI') ORDER BY TO_CHAR (X_RQST_DATE, 'HH24:MI') DESC";
                break;
            case "esnPurchaseHistory":
                if (qParameter.matches("^[0-9]+$"))
                {
                    theQuery = "SELECT x_esn, objid, x_rqst_source, x_rqst_type, x_merchant_id, x_ics_rcode, x_ics_rflag, x_ics_rmsg, TO_CHAR(x_rqst_date,'MON-DD-YYYY HH24:MM:SS') \"Date\", X_CUSTOMER_EMAIL FROM table_x_purch_hdr WHERE x_rqst_date >= trunc(sysdate) and X_ESN ='" + qParameter + "' ORDER BY \"Date\"";
                } else
                {
                    throw new IllegalArgumentException("Need a valid ESN");
                }
                break;
            case "emailPurchaseHistory":
                if (EmailValidator.getInstance().isValid(qParameter))
                {
                    theQuery = "SELECT X_esn, objid,x_rqst_source, x_rqst_type, x_merchant_id, x_ics_rcode, x_ics_rflag, x_ics_rmsg, TO_CHAR(x_rqst_date,'MON-DD-YYYY HH24:MM:SS') \"Date\", X_CUSTOMER_EMAIL  FROM table_x_purch_hdr  WHERE x_rqst_date >= trunc(sysdate)  AND X_CUSTOMER_EMAIL ='" + qParameter + "' ORDER BY \"Date\"";
                } else
                {
                    throw new IllegalArgumentException("Need a valid email");
                }
                break;

            //QUERIES FOR AUTOMATION
            case "automationAllTrans":
                theQuery = "select to_char(b.TRANS_DATE,'MON-DD-YYYY') t_date, b.source channel, b.total total, substr((b.total/a.total)*100,1,4) Percent from ( select trunc(ct.x_transact_date) TRANS_DATE ,count(*) total \n" +
                            " from table_x_call_trans ct where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type in ('1','3','6') and x_result||''='Completed' \n" +
                            " group by trunc(ct.x_transact_date) ) a, (select trunc(ct.x_transact_date) TRANS_DATE ,Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) source ,count(*) total \n" +
                            " from table_x_call_trans ct where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type in ('1','3','6') and x_result||''='Completed' \n" +
                            " group by trunc(ct.x_transact_date), decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem) ) b where a.TRANS_DATE = b.TRANS_DATE";
                break;
            case "automationAllTransSpecificDate":
                theQuery = "select to_char(b.TRANS_DATE,'DD-MON-YY') t_date, b.source channel, b.total total, substr((b.total/a.total)*100,1,4) Percent from ( select trunc(ct.x_transact_date) TRANS_DATE ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(to_date('" + qParameter + "')) and ct.x_transact_date < trunc(to_date('" + qParameter + "'))+1 and ct.x_action_type in ('1','3','6') and x_result||''='Completed' group by trunc(ct.x_transact_date) ) a, (select trunc(ct.x_transact_date) TRANS_DATE ,Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) source ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(to_date('" + qParameter + "')) and ct.x_transact_date < trunc(to_date('" + qParameter + "'))+1 and ct.x_action_type in ('1','3','6') and x_result||''='Completed' group by trunc(ct.x_transact_date), decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem) ) b where a.TRANS_DATE = b.TRANS_DATE";
                break;
            case "automationActivations":
                theQuery = "select to_char(b.TRANS_DATE,'MON-DD-YYYY') t_date, b.source channel, b.total total, substr((b.total/a.total)*100,1,4) Percent from ( select trunc(ct.x_transact_date) TRANS_DATE ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type in ('1','3') and x_result||''='Completed' group by trunc(ct.x_transact_date) ) a, (select trunc(ct.x_transact_date) TRANS_DATE ,Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) source ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type in ('1','3') and x_result||''='Completed' group by trunc(ct.x_transact_date), decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem) ) b where a.TRANS_DATE = b.TRANS_DATE";
                break;
            case "automationActivationsSpecificDate":
                theQuery = "select to_char(b.TRANS_DATE,'DD-MON-YY') t_date, b.source channel, b.total total, substr((b.total/a.total)*100,1,4) Percent from ( select trunc(ct.x_transact_date) TRANS_DATE ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(to_date('" + qParameter + "')) and ct.x_transact_date < trunc(to_date('" + qParameter + "'))+1 and ct.x_action_type in ('1','3') and x_result||''='Completed' group by trunc(ct.x_transact_date) ) a, (select trunc(ct.x_transact_date) TRANS_DATE ,Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) source ,count(*) total from table_x_call_trans ct where ct.x_transact_date >= trunc(to_date('" + qParameter + "')) and ct.x_transact_date < trunc(to_date('" + qParameter + "'))+1 and ct.x_action_type in ('1','3') and x_result||''='Completed' group by trunc(ct.x_transact_date), decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem) ) b where a.TRANS_DATE = b.TRANS_DATE";
                break;
            case "automationRedemptions":
                theQuery = "select to_char(x_transact_date,'MON-DD-YYYY') t_date, Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) channel, count(*) total, round(count(*)/(sum(count(*)) over()),4)*100 percent from table_x_call_trans ct where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type||'' = '6' and x_result||''='Completed' group by decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem), trunc(ct.x_transact_date), to_char(x_transact_date,'MON-DD-YYYY'), Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) order by decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)";
                break;
            case "automationRedepmtionsSpecificDate":
                theQuery = "select to_char(x_transact_date,'DD-MON-YY') t_date, Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) channel, count(*) total, round(count(*)/(sum(count(*)) over()),4)*100 percent from table_x_call_trans ct where ct.x_transact_date >= trunc(to_date('" + qParameter + "')) and ct.x_transact_date < trunc(to_date('" + qParameter + "'))+1 and ct.x_action_type||'' = '6' and x_result||''='Completed' group by decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem), trunc(ct.x_transact_date), to_char(x_transact_date,'DD-MON-YY'), Upper(decode(ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)) order by decode (ct.x_sourcesystem,'Clarify','Agent','WEBCSR','Agent',ct.x_sourcesystem)";
                break;
            case "automationSuccess":
                theQuery = "select b.t_hour \"Date\",b.x_action_text,b.x_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select distinct to_char(x_transact_date,'MON-DD-YYYY') t_hour from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'MON-DD-YYYY') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'MON-DD-YYYY'), x_action_text,x_sourcesystem) b, (select to_char(x_transact_date,'MON-DD-YYYY') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'MON-DD-YYYY'), x_action_text,x_sourcesystem) c where b.x_sourcesystem = c.x_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour, b.x_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessSpecificDate":
                theQuery = "select b.t_hour,b.x_action_text,b.x_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) success_rate from ( select distinct to_char(x_transact_date,'MM-DD') t_hour from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "'))+1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'MM-DD') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "'))+1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'MM-DD'), x_action_text,x_sourcesystem) b, (select to_char(x_transact_date,'MM-DD') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "'))+1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'MM-DD'), x_action_text,x_sourcesystem) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour, b.x_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessHr":
                theQuery = "select b.t_hour,b.x_action_text,b.x_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select distinct to_char(x_transact_date,'HH24') t_hour from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) b, (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) c where b.x_sourcesystem = c.x_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour desc, b.x_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessHrSpecificDate":
                theQuery = "select a.t_date, b.t_hour,b.x_action_text,b.x_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) success_rate from ( select distinct to_char(x_transact_date,'HH24') t_hour, to_char(x_transact_date,'DD-MON-YY') t_date from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "')) +1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "')) +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) b, (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(to_date('" + qParameter + "')) and x_transact_date < trunc(to_date('" + qParameter + "')) +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour desc, b.x_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessCurrentHr":
                theQuery = "select b.t_hour,b.x_action_text,b.x_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select distinct to_char(x_transact_date,'HH24') t_hour from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) b, (select to_char(x_transact_date,'HH24') t_hour, x_action_text, x_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'HH24'), x_action_text,x_sourcesystem) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour desc, b.x_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessAllByBrand":
                theQuery = "select b.t_hour current_hour, b.x_action_text action, b.x_sourcesystem sourcesystem, b.x_sub_sourcesystem sub_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select distinct to_char(x_transact_date,'HH24') t_hour from table_x_call_trans where x_transact_date >= trunc(sysdate) and x_transact_date < trunc(sysdate) +1 and x_action_type in ('1','3','6')	) a ,	( select to_char(x_transact_date,'HH24') t_hour,	x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate) and x_transact_date < trunc(sysdate) +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'HH24'), x_action_text, x_sourcesystem, x_sub_sourcesystem	) b, ( select to_char(x_transact_date,'HH24') t_hour,	x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate) and x_transact_date < trunc(sysdate) +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'HH24'),	x_action_text,	x_sourcesystem,	x_sub_sourcesystem	) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_sub_sourcesystem = c.x_sub_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour desc, b.x_sourcesystem,b.x_sub_sourcesystem, b.x_action_text";
                break;
            case "automationSuccessCurrentByBrand":
                theQuery = "select b.t_hour current_hour, b.x_action_text action, b.x_sourcesystem sourcesystem, b.x_sub_sourcesystem sub_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select distinct to_char(x_transact_date,'HH24') t_hour from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6')) a , (select to_char(x_transact_date,'HH24') t_hour,x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6') group by to_char(x_transact_date,'HH24'), x_action_text, x_sourcesystem, x_sub_sourcesystem) b, (select to_char(x_transact_date,'HH24') t_hour,x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= trunc(sysdate,'HH') and x_transact_date < trunc(sysdate,'HH') +1 and x_action_type in ('1','3','6') and x_result = 'Completed' group by to_char(x_transact_date,'HH24'),x_action_text,x_sourcesystem,x_sub_sourcesystem) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_sub_sourcesystem = c.x_sub_sourcesystem(+) and b.x_action_text = c.x_action_text(+) and b.t_hour = c.t_hour (+) and a.t_hour = b.t_hour order by b.t_hour desc, b.x_sourcesystem, b.x_sub_sourcesystem, b.x_action_text";
                break;
            case "automationSuccess20min":
                theQuery = "select b.x_action_text action, b.x_sourcesystem sourcesystem, b.x_sub_sourcesystem sub_sourcesystem, b.total total, c.total completed, substr(to_char((c.total/b.total)*100),1,4) \"Success Rate (%)\" from ( select x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= (sysdate-20/1440) and x_transact_date < (sysdate) and x_action_type in ('1','3','6') group by x_action_text, x_sourcesystem, x_sub_sourcesystem	) b, ( select x_action_text, x_sourcesystem, x_sub_sourcesystem, count(*) Total from table_x_call_trans where x_transact_date >= (sysdate-20/1440) and x_transact_date < (sysdate) and x_action_type in ('1','3','6') and x_result = 'Completed' group by x_action_text,	x_sourcesystem,	x_sub_sourcesystem	) c where 1=1 and b.x_sourcesystem = c.x_sourcesystem(+) and b.x_sub_sourcesystem = c.x_sub_sourcesystem(+) and b.x_action_text = c.x_action_text(+) order by b.x_sourcesystem,	b.x_sub_sourcesystem, b.x_action_text";
                break;
            case "automationDealer20min":
                theQuery = "select ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem, sum(decode(ct.x_result,'Completed',1,0)) completed, count(*) total, round((sum(decode(ct.x_result,'Completed',1,0))/count(*))*100,2) \"Success Rate (%)\" from table_x_call_trans ct where ct.x_transact_date >= (sysdate-20/1440) and ct.x_transact_date < (sysdate) and ct.x_action_type in ('1','3','6') and exists(select 1 from table_user u where u.objid = CT.X_CALL_TRANS2USER and u.login_name like '%@%') group by ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem";
                break;
            case "automationDealer1hr":
                theQuery = "select ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem, sum(decode(ct.x_result,'Completed',1,0)) completed, count(*) total, round((sum(decode(ct.x_result,'Completed',1,0))/count(*))*100,2) \"Success Rate (%)\" from table_x_call_trans ct where ct.x_transact_date >= (sysdate-60/1440) and ct.x_transact_date < (sysdate) and ct.x_action_type in ('1','3','6') and exists(select 1 from table_user u where u.objid = CT.X_CALL_TRANS2USER and u.login_name like '%@%') group by ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem";
                break;
            case "automationDealerAllDay":
                theQuery = "SELECT ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem, SUM (DECODE (ct.x_result, 'Completed', 1, 0))completed, COUNT (*) total, ROUND ( (SUM (DECODE (ct.x_result, 'Completed', 1, 0)) / COUNT (*)) * 100, 2) \"Success Rate (%)\" FROM table_x_call_trans ct WHERE ct.x_transact_date >=trunc (SYSDATE) AND ct.x_transact_date <trunc (SYSDATE) + 1  AND ct.x_action_type IN ('1', '3', '6')  AND EXISTS (SELECT 1 FROM table_user u WHERE u.objid = CT.X_CALL_TRANS2USER AND u.login_name LIKE '%@%') GROUP BY ct.x_action_text, ct.x_sourcesystem, ct.x_sub_sourcesystem";
                break;

            //QUERIES FOR CARRIER SUCCESS RATES
            case "carrierActivations20min":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) b, (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierActivations1hr":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) b, (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierActivationsAllDay":
                theQuery = "SELECT b.t_hour, b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT TO_CHAR(x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY TO_CHAR(x_transact_date,'HH24'), pa.x_parent_name ) b, (SELECT TO_CHAR(x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('1','3') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY TO_CHAR(x_transact_date,'HH24'), pa.x_parent_name ) c WHERE 1 =1 AND b.t_hour = c.t_hour(+) AND b.parent_name= c.parent_name(+) ORDER BY b.t_hour DESC, b.parent_name";
                break;
            case "carrierRedemptions20min":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) b, (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierRedemptions1hr":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) b, (SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY pa.x_parent_name ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierRedemptionsAllDay":
                theQuery = "SELECT b.t_hour, b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM (SELECT TO_CHAR(x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY TO_CHAR(x_transact_date,'HH24'), pa.x_parent_name ) b, (SELECT TO_CHAR(x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('6') AND ct.x_result = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group GROUP BY TO_CHAR(x_transact_date,'HH24'), pa.x_parent_name ) c WHERE 1 =1 AND b.t_hour = c.t_hour(+) AND b.parent_name= c.parent_name(+) ORDER BY b.t_hour DESC, b.parent_name";
                break;

            //QUERIES FOR CARRIER SUCCESS OTA RATES
            case "carrierActivationsOTA20min":
                theQuery = " SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) b, ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') and ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierActivationsOTA1hr":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) b, ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('1','3') and ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierActivationsOTAAllDay":
                theQuery = "SELECT b.t_hour,b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT TO_CHAR(ct.x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('1','3') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans GROUP BY TO_CHAR(ct.x_transact_date,'HH24'),pa.x_parent_name ORDER BY TO_CHAR(ct.x_transact_date,'HH24') desc ) b, ( SELECT TO_CHAR(ct.x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('1','3') AND ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans GROUP BY TO_CHAR(ct.x_transact_date,'HH24'),pa.x_parent_name ORDER BY TO_CHAR(ct.x_transact_date,'HH24') desc ) c WHERE 1 =1 AND b.t_hour = c.t_hour(+) AND b.parent_name= c.parent_name(+) ORDER BY b.t_hour DESC, b.parent_name ";
                break;
            case "carrierRedemptionsOTA20min":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) b, ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-20/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') and ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierRedemptionsOTA1hr":
                theQuery = "SELECT b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) b, ( SELECT pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE ct.x_transact_date >= (sysdate-60/1440) AND ct.x_transact_date < (sysdate) AND ct.x_action_type IN ('6') and ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans group by pa.x_parent_name order by pa.x_parent_name desc ) c WHERE 1 =1 AND b.parent_name= c.parent_name(+) ORDER BY b.parent_name";
                break;
            case "carrierRedemptionsOTAAllDay":
                theQuery = "SELECT b.t_hour,b.parent_name carrier, b.total total, c.total completed, SUBSTR(TO_CHAR((c.total/b.total)*100),1,4) success_rate FROM ( SELECT TO_CHAR(ct.x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('6') AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans GROUP BY TO_CHAR(ct.x_transact_date,'HH24'),pa.x_parent_name ORDER BY TO_CHAR(ct.x_transact_date,'HH24') desc ) b, ( SELECT TO_CHAR(ct.x_transact_date,'HH24') t_hour, pa.x_parent_name parent_name, COUNT(*) Total FROM table_x_call_trans ct, table_x_parent pa, TABLE_X_CARRIER ca, TABLE_X_CARRIER_GROUP cga, table_x_ota_transaction ot, table_x_ota_trans_dtl dt WHERE 1 =1 AND ct.x_transact_date >= TRUNC(sysdate) AND ct.x_transact_date < TRUNC(sysdate) +1 AND ct.x_action_type IN ('6') AND ot.x_status = 'Completed' AND ct.x_call_trans2carrier = ca.objid AND pa.objid = cga.x_carrier_group2x_parent AND cga.objid = ca.carrier2carrier_group AND ct.objid = ot.X_OTA_TRANS2X_CALL_TRANS AND ot.objid =dt.x_ota_trans_dtl2x_ota_trans GROUP BY TO_CHAR(ct.x_transact_date,'HH24'),pa.x_parent_name ORDER BY TO_CHAR(ct.x_transact_date,'HH24') desc ) c WHERE 1 =1 AND b.t_hour = c.t_hour(+) AND b.parent_name= c.parent_name(+) ORDER BY b.t_hour DESC, b.parent_name";
                break;

            //QUERIES FOR BRM
            case "brmPOJO":
                theQuery = "SELECT TO_CHAR (DEQUEUE_TIMESTAMP, 'HH24') HOUR,SOURCESYSTEM source,IG_ORDER_TYPE type,ACTION_TEXT action,COUNT(*) COUNT FROM sa.x_queue_event_log WHERE DEQUEUE_TIMESTAMP >= (SYSDATE) - 1 GROUP BY SOURCESYSTEM,IG_ORDER_TYPE,ACTION_TEXT,TO_CHAR (DEQUEUE_TIMESTAMP, 'HH24') ORDER BY 1 ASC";
                break;
            case "brmESNLookup":
                theQuery = "SELECT OBJID,ESN,EVENT_NAME event,MIN,MSID,QUEUE_EVENT_LOG_STATUS status,ACTION_TEXT action,ACTION_TYPE \"action type\",BUS_ORG_ID brand,ENQUEUE_OUTPUT_MESSAGE output, to_char(INSERT_TIMESTAMP,'MON-DD-YYYY HH24:MM:SS') inserted, to_char(UPDATE_TIMESTAMP,'MON-DD-YYYY HH24:MM:SS') updated FROM SA.x_queue_event_log WHERE esn = ('" + qParameter + "') order by 1";
                break;

            //QUERIES FOR CASES
            case "openCases":
                theQuery = "SELECT decode(ca.x_case_type, 'Carrier LA','Line Activation Cases','Carrier LA Features','Feature Cases','Carrier LM','Line Management Cases','Line Activation','Line Activation Cases','Line Management','Line Management Cases','Features','Feature Cases','Other','ERD Cases','Port In','Port Cases','Port Out','Port Cases') CASE_TYPE,count(*) TOTAL_OPEN FROM table_case ca, table_condition co WHERE ca.case_state2condition = co.objid AND co.s_title ||'' != 'CLOSED' AND ca.x_case_type in ('Carrier LA','Carrier LA Features','Carrier LM','Line Activation','Line Management','Features','Other','Port In','Port Out') AND ca.creation_time >= trunc(sysdate-7) AND ca.creation_time <= trunc(sysdate+1) GROUP BY decode(ca.x_case_type, 'Carrier LA','Line Activation Cases','Carrier LA Features','Feature Cases','Carrier LM','Line Management Cases','Line Activation','Line Activation Cases','Line Management','Line Management Cases','Features','Feature Cases','Other','ERD Cases','Port In','Port Cases','Port Out','Port Cases') ORDER BY TOTAL_OPEN desc";
                break;
            case "createdWeb":
                theQuery = "Select to_char(tc.creation_time,'MON-DD-YYYY') MY_DATE, count(*) TOTAL From table_case tc Where tc.creation_time >= trunc(sysdate) and tc.creation_time < trunc(sysdate)+1 and tc.case_originator2user+0 in (268462098, 268494225) group by to_char(tc.creation_time,'MON-DD-YYYY') order by to_char(tc.creation_time,'MON-DD-YYYY')";
                break;
            case "createdWebHr":
                theQuery = "Select to_char(tc.creation_time,'MON-DD-YYYY HH24') MY_DATE, count(*) TOTAL From table_case tc Where tc.creation_time >= trunc(sysdate) and tc.creation_time < trunc(sysdate)+1 and tc.case_originator2user+0 in (268462098, 268494225) group by to_char(tc.creation_time,'MON-DD-YYYY HH24') order by to_char(tc.creation_time,'MON-DD-YYYY HH24')";
                break;
            case "transactionTypeHr":
                theQuery = "select x_tran_type TRANSACTION_TYPE, count(*) COUNT from table_x_webcsr_log a, (select x_esn, max(objid) last_objid from table_x_webcsr_log where x_date_time >= (sysdate-60/1440) and x_agent != 'cbo' group by x_esn) b where a.objid=b.last_objid group by x_tran_type order by count(*) desc";
                break;
            case "warehouseRecurring":
                String start = qParameter.substring(0, qParameter.indexOf("_"));
                String end = qParameter.substring(qParameter.indexOf("_") + 1, qParameter.lastIndexOf("_"));
                String count = qParameter.substring(qParameter.lastIndexOf("_") + 1);
                if (Integer.parseInt(count) < 2)
                {
                    count = "2";
                }
                theQuery = "select id_number CASE_ID, count(*) total from table_phone_log, table_case, table_gbst_elm \n" +
                            "where table_phone_log.creation_time between TO_DATE('" + start + "', 'YYYY-MM-DD') and To_DATE('" + end + "', 'YYYY-MM-DD') and \n" +
                            "action_type = 'Auto Log' and table_case.objid = case_phone2case and table_gbst_elm.objid = casests2gbst_elm and table_gbst_elm.title <> 'Closed' \n" +
                            "group by id_number having count(*) > " + count + " order by total DESC";
                break;

            // QUERIES FOR CBO ERROR LOOKUP
            case "esnCboError":
                theQuery = "select X_ESN_IMEI ESN, OBJID, X_SOURCE_SYSTEM SOURCE, X_CBO_METHOD METHOD, X_ERROR_STRING ERROR, to_char(X_ERROR_DATE,'MON-DD-YYYY HH24:MM:SS') T_DATE from table_x_cbo_error where x_esn_IMEI=\'" + qParameter + "\' ORDER BY X_ERROR_DATE DESC";
                break;
                
            // QUERIES FOR CBO SCHEDULER
            case "cboScheduler5Min":
                theQuery = "SELECT cbo_task_name TASK, status, count(*) TNUM FROM table_queued_cbo_service WHERE CREATION_DATE > (sysdate) -5/1440 GROUP BY cbo_task_name, status";
                break;
            case "cboScheduler30Min":
                theQuery = "SELECT cbo_task_name TASK, status, count(*) TNUM FROM table_queued_cbo_service where CREATION_DATE > (sysdate) -30/1440 group by cbo_task_name, status";
                break;
            case "cboScheduler1Hour":
                theQuery = "SELECT cbo_task_name TASK, status, count(*) TNUM FROM table_queued_cbo_service where CREATION_DATE > (sysdate) -1/24 group by cbo_task_name, status";
                break;
            case "cboScheduler24Hours":
                theQuery = "SELECT cbo_task_name TASK, status, count(*) TNUM FROM table_queued_cbo_service where CREATION_DATE > (sysdate) - 1 group by cbo_task_name, status";
                break;

            // QUERIES FOR ILD & Data Service
            case "ildSummary":
                theQuery = "select x_ild_trans_type ORDER_TYPE, x_ild_status STATUS, count(*) TOTALCOUNT from sa.table_x_ild_transaction where 1=1 and x_ild_trans_type in ('ILD_CREATE','ILD_DEACT') and x_transact_date >=trunc(sysdate) group by x_ild_trans_type, x_ild_status order by x_ild_trans_type, x_ild_status";
                break;
            case "ild10LastHour":
                theQuery = "select x_merchant_id BRAND,x_ics_rflag CARD_STATUS ,count(*) MYCOUNT from table_x_purch_hdr where 1=1 and x_rqst_date >= sysdate-60/1440 and x_esn in ( select x_esn FROM table_x_ild_transaction WHERE 1=1 and x_transact_date >= sysdate-60/1440 and x_product_id like '%ILD_10%' ) group by x_merchant_id, x_ics_rflag";
                break;
            case "ciCacheInvalidateDailySummary":
                theQuery = "SELECT DECODE(x_status,'Q', 'IN DB2W3CI QUEUE','L','WAITING FOR 3CI',x_status) STATUS, COUNT(*) COUNT FROM W3CI.W3CI_INV_CACHE_ESNS WHERE creation_date>= TRUNC(sysdate) GROUP BY DECODE(x_status,'Q', 'IN DB2W3CI QUEUE','L','WAITING FOR 3CI',x_status)";
                break;
            case "ildDataServiceESNLookUp":
                theQuery = "select X_ESN, X_MIN, to_char(X_TRANSACT_DATE,'MON-DD-YYYY HH24:MM:SS') X_TRANSACT_DATE, X_ILD_TRANS_TYPE, X_ILD_STATUS, to_char(X_LAST_UPDATE,'MON-DD-YYYY HH24:MM:SS') X_LAST_UPDATE, X_PRODUCT_ID, X_API_STATUS, X_API_MESSAGE from sa.table_x_ild_transaction where x_transact_date >= trunc(sysdate-1) and x_esn=\'" + qParameter + "\'";
                break;
            case "ildDataServiceMINLookUp":
                theQuery = "select X_ESN, X_MIN, to_char(X_TRANSACT_DATE,'MON-DD-YYYY HH24:MM:SS') X_TRANSACT_DATE, X_ILD_TRANS_TYPE, X_ILD_STATUS, to_char(X_LAST_UPDATE,'MON-DD-YYYY HH24:MM:SS') X_LAST_UPDATE, X_PRODUCT_ID, X_API_STATUS, X_API_MESSAGE from sa.table_x_ild_transaction where x_transact_date >= trunc(sysdate-1) and x_min=\'" + qParameter + "\'";
                break;
                
            // QUERIES FOR INTERGATE
            case "intergateSummary":
                theQuery = "select sum(valid) Total, round(sum(success)/sum(valid),2)*100 AS \"Success Rate (%)\" from ( select action_item_id, template, nvl(max(decode(status,'F',1,'E',1)),0) Failure, nvl(max(decode(status,'S',1,'W',1)),0) success, 1 valid from gw1.ig_transaction where update_date>trunc(sysdate) and transmission_method||''='AOL' and status ||'' in ('S','W','F','E') group by action_item_id, template )";
                break;
            case "pendingByTemplate":
                theQuery = "select template, decode(status, 'HE','Hold Error','HW','Hold Worked','L','Processing','R','Pending','Q','Pending','CP','Pending Response From Carrier','CPU','Pending Response From Carrier') status, count(template) as \"Transactions\", to_char(min(creation_date), 'MON/DD HH24:MI') AS \"Creation Date\", to_char(min(update_date), 'MON/DD HH24:MI') AS \"Last enter intergate\" from gw1.ig_transaction where creation_date>trunc(sysdate) and status in ('HE','Hold Error','HW','Hold Worked','L','Processing','R','Pending','Q','Pending','CP','Pending Response From Carrier','CPU','Pending Response From Carrier') and transmission_method||''='AOL' and order_type not in ('D','S', 'BI') group by template, decode(status, 'HE','Hold Error','HW','Hold Worked','L','Processing','R','Pending','Q','Pending','CP','Pending Response From Carrier','CPU','Pending Response From Carrier') order by \"Transactions\" desc";
                break;
            case "systemSuccessRate":
                theQuery = "SELECT decode(transmission_method,'AOL','API','EMAIL','EMAIL') Template, sum(valid) Total, round(sum(success)/sum(valid),2)*100 AS \"Success Rate (%)\" \n" +
                            "from (select action_item_id, transmission_method, nvl(max(decode(status,'F',1,'E',1)),0) Failure, nvl(max(decode(status,'S',1,'W',1)),0) success, 1 valid \n" +
                            "from gw1.ig_transaction where (creation_date>=trunc(sysdate) or update_date>=trunc(sysdate)) and transmission_method||'' in ('AOL','EMAIL') and status||'' in ('S','W','F','E') \n" +
                            "group by action_item_id, transmission_method ) group by decode(transmission_method,'AOL','API','EMAIL','EMAIL')";
                break;
            case "systemErrors":
                theQuery = "select template, count(distinct action_item_id) as Count, status_message from gw1.ig_transaction where update_date>trunc(sysdate) and transmission_method||''='AOL' and status ||'' in ('F','E') group by template, status_message order by Count desc";
                break;
            case "intergate_tat":
                theQuery = "select TRANSTAT.TEMPLATE, TO_CHAR(TRANSTAT.x_transact_date, 'MM/DD/YY') AS \"DATE\", COUNT(*) TOTAL, SUM(CASE WHEN IG_TAT_TIME BETWEEN '00:00:00' AND '00:00:30' THEN 1 ELSE 0 END) \"IG 30 Secs\", \n" +
                            " round( SUM(CASE WHEN IG_TAT_TIME BETWEEN '00:00:00' AND '00:00:30' THEN 1 ELSE 0 END) /count(*)*100,1) \"IG 30 Secs%\", SUM(CASE WHEN IG_TAT_TIME BETWEEN '00:00:30' AND '00:01:00' THEN 1 ELSE 0 END) \n" +
                            " \"IG 30 Secs - 1 Min\", SUM(CASE WHEN IG_TAT_TIME BETWEEN '00:01:00' AND '00:01:30' THEN 1 ELSE 0 END) \"IG 1 - 1:30 Min\", SUM(CASE WHEN IG_TAT_TIME > '00:01:30' THEN 1 ELSE 0 END) \"IG >1:30 Minutes\", \n" +
                            " SUM(CASE WHEN CALL_TRANS2IG_DAYS_TIME BETWEEN '00:00:00' AND '00:00:30' THEN 1 ELSE 0 END) \"CT2IG 30 Secs\", round( SUM(CASE WHEN CALL_TRANS2IG_DAYS_TIME BETWEEN '00:00:00' AND '00:00:30' THEN 1 ELSE 0 END) /count(*)*100,1) \n" +
                            " \"CT2IG 30 Secs%\", SUM(CASE WHEN CALL_TRANS2IG_DAYS_TIME BETWEEN '00:00:30' AND '00:01:00' THEN 1 ELSE 0 END) \"CT2IG 30 Secs - 1 Min\", SUM(CASE WHEN CALL_TRANS2IG_DAYS_TIME BETWEEN '00:01:00' AND '00:01:30' THEN 1 ELSE 0 END) \n" +
                            " \"CT2IG 1 - 1:30 Min\", SUM(CASE WHEN CALL_TRANS2IG_DAYS_TIME > '00:01:30' THEN 1 ELSE 0 END) \"CT2IG <1:30 Minutes\" from (select c.template, e.x_service_id, c.STATUS, c.ORDER_TYPE, e.x_transact_date, \n" +
                            " min(c.CREATION_DATE) MIN_DATE, max(c.UPDATE_DATE) MAX_UPDATE_DATE, TO_NUMBER( TO_CHAR( TO_DATE('1','J') + (min(c.creation_date) - e.x_transact_date), 'J')-1) CALL_TRANS2IG_DAYS, \n" +
                            " TO_CHAR( TO_DATE('00:00:00','HH24:MI:SS') + (min(c.creation_date) - e.x_transact_date), 'HH24:MI:SS') CALL_TRANS2IG_DAYS_TIME, TO_NUMBER( TO_CHAR( TO_DATE('1','J') + (max(c.update_date) - min(c.creation_date)), 'J')-1) \n" +
                            " IG_TAT_DAYS, TO_CHAR( TO_DATE('00:00:00','HH24:MI:SS') + (max(c.update_date) - min(c.creation_date)), 'HH24:MI:SS') IG_TAT_TIME from table_x_call_trans e, table_task d, IG_TRANSACTION C where 1=1 \n" +
                            " and e.objid = d.x_task2x_call_trans and d.task_id = c.ACTION_ITEM_ID and c.status = 'S' AND C.CARRIER_ID != '999999' and c.transmission_method='AOL' and e.x_result = 'Completed' and e.x_action_type IN ('3','1') \n" +
                            " and E.X_TRANSACT_DATE > SYSDATE-1/24 group by c.template, e.x_service_id, c.STATUS, c.ORDER_TYPE, e.x_transact_date order by E.X_TRANSACT_DATE,min(creation_date), x_service_id ) TRANSTAT \n" +
                            " GROUP BY TRANSTAT.TEMPLATE,TO_CHAR(TRANSTAT.x_transact_date, 'MM/DD/YY') ORDER BY TRANSTAT.TEMPLATE,TO_CHAR(TRANSTAT.x_transact_date, 'MM/DD/YY')";
                break;
            case "carrierErrorLast30Min":
                theQuery = "select template, order_type \"Order Type\", status, status_message \"Status Message\", count(*) COUNT from ig_transaction where creation_date >= (sysdate-3/144) and creation_date <= (sysdate) and status in ('E','F') group by template, order_type, status, status_message";
                break;
            case "intergateStatusLast30Min":
                theQuery = "select template, order_type \"ORDER TYPE\", status, status_message \"STATUS MESSAGE\", count(*) COUNT from ig_transaction where creation_date >= (sysdate-3/144) and creation_date <= (sysdate) group by template, order_type, status, status_message";
                break;
            case "surepayActivationStatus24Hours":
                theQuery = "select order_type, status, status_message, count from (select template, order_type, status, status_message, count(*) as count from ig_transaction where creation_date >= (sysdate-1) and creation_date <= (sysdate) and template='SUREPAY' and order_type in ('AP','CR','A') group by template, order_type, status, status_message)";
                break;
            case "surepayActivationStatus30Minutes":
                theQuery = "select order_type, status, status_message, count from (select template, order_type, status, status_message, count(*) as count from ig_transaction where creation_date >= (sysdate-3/144) and creation_date <= (sysdate) and template='SUREPAY' and order_type in ('AP','CR','A') group by template, order_type, status, status_message)";
                break;
            case "intergateCheckByOrderType":
                theQuery = "Select template, order_type, count(*) COUNT from ig_transaction where status='W' GROUP BY template,order_type";
                break;
            case "intergateIGLookupTransactionID":
                theQuery = "select action_item_id, esn, to_char(creation_date,'MON-DD-YYYY HH24:MM:SS') creation_date, to_char(update_date,'MON-DD-YYYY HH24:MM:SS') update_date, min, order_type, status, q_transaction, msid, status_message from gw1.ig_transaction where creation_date>trunc(sysdate-1) and transaction_id='" + qParameter + "'";
                break;
            case "intergateIGLookupESN":
                theQuery = "select action_item_id, transaction_id, to_char(creation_date,'MON-DD-YYYY HH24:MM:SS') creation_date, to_char(update_date,'MON-DD-YYYY HH24:MM:SS') update_date, min, order_type, status, q_transaction, msid, status_message from gw1.ig_transaction where creation_date>trunc(sysdate-1) and esn='" + qParameter + "'";
                break;
            case "actionIDCreationTime":
                theQuery = "select template,action_item_id, esn, order_type, to_char(creation_date,'MON-DD-YYYY HH24:MM:SS') creation_date from gw1.ig_transaction where creation_date>trunc(sysdate-1) and action_item_id='" + qParameter + "'";
                break;
            case "tankLookup":
                theQuery = "select to_char(X_REQ_DATE_TIME,'MON-DD-YYYY HH24:MM:SS') X_REQ_DATE_TIME, X_SOURCESYSTEM, X_DEPOSIT from table_x_zero_out_max where x_esn='" + qParameter + "' and x_req_date_time>trunc(sysdate) order by x_req_date_time";
                break;

            // QUERIES FOR OEM UNLOCK
            case "poCount":
                theQuery = "select COUNT(ESN) \"COUNT\" from SA.UNLOCK_SPC_ENCRYPT where PO = '" + qParameter + "'";
                break;
            case "oemUnlockESNLookup":
                theQuery = "select ESN, PO, UNLOCK_STATUS from SA.UNLOCK_SPC_ENCRYPT where ESN in (" + qParameter + ")";
                break;

            // QUERIES FOR OTA
            case "OTASummary":
                theQuery = "select to_char(runtime_date,'HH24:MI') time,red_total TOTAL,ROUND(red_total_succ_rate,2) \"SUCC RATE (%)\",ROUND(avgtime_red_total_succ) AS \"AVG TIME\",act_total TOTAL,ROUND(act_total_succ_rate,2) AS \"SUCC RATE (%)\",ROUND(avgtime_act_total_succ) AS \"AVG TIME\", react_total TOTAL,ROUND(react_total_succ_rate,2) AS \"SUCC RATE (%)\",ROUND(avgtime_react_total_succ) AS \"AVG TIME\",per_total TOTAL,ROUND(per_total_succ_rate,2) AS \"SUCC RATE (%)\",ROUND(avgtime_per_total_succ) AS \"AVG TIME\" from rt_trans_query where runtime_date>= trunc(sysdate) order by time desc";
                break;
            case "OTAMTSuccessRate":
                theQuery = "select to_char(report_end_date,'HH24:MI') TIME,ROUND(pct_resp_to_mt, 2) AS \"RESP to MT (%)\",ROUND(avg_mt_w_mo_u2, 2) AS \"MT plus MO under 2 MIN (%)\",ROUND(pct_success_to_mt, 2) AS \"MT Success (%)\" from otaadm.mt_mo_messgs where report_start_date >= trunc(sysdate) order by report_start_date desc";
                break;
            case "OTAMOSuccessRate":
                theQuery = "select to_char(report_end_date,'HH24:MI') TIME,ROUND(pct_resp_to_mo, 2) AS \"RESP to MO (%)\", ROUND(avg_mo_w_mt_u2, 2) AS \"MT plus MO under 2 MIN (%)\" from otaadm.mo_mt_messgs where report_start_date >= trunc(sysdate) order by report_start_date desc";
                break;
            case "MOsLastMinute":
                theQuery = "select substr(m365_id,1,4) NAME,count(*) COUNT from psms_log where message_timestamp >= (sysdate-1/1440) and substr(original_msg,1,6) != '/ //TF' group by substr(m365_id,1,4)";
                break;
            case "currentOTABatchStatus":
                theQuery = "SELECT status, COUNT (*) AS \"COUNT\" FROM sa.x_ota_batch_detail GROUP BY status";
                break;
            case "moTodayTMobile":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) = 'tmxr' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) + 1 AND messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "moTodayVerizon":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 2) = 'vz' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 AND messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "moTodayCingular":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) = 'cgsr' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "moTodayUSCellular":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) = 'usrx' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "moTodayClaroSybase":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) = 'clsr' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) + 1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "3ciTodayTMobile":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) like '3C02' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "3ciTodayVerizon":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 5) like '3C030' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "3ciTodayATT":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) like '3C00' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) +1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "3ciTodayClaro":
                theQuery = "select to_char(message_timestamp, 'HH24:MI') as time, count(*) as count from psms_log where substr(m365_id, 1, 4) = '3C14' AND message_timestamp >= TRUNC(sysdate) AND message_timestamp < TRUNC(sysdate) + 1 and messg_dir='MO' group by to_char(message_timestamp, 'HH24:MI') ORDER by to_char(message_timestamp, 'HH24:MI') desc";
                break;
            case "smsStatsLastMin":
                theQuery = "select messg_dir AS \"Message Dir\",sms_name AS \"SMS Name\",count(*) count from psms_log where MESSAGE_TIMESTAMP >= sysdate-1/1440 group by messg_dir, sms_name order by messg_dir,sms_name desc";
                break;
            case "smsStatsLast20Min":
                theQuery = "select messg_dir AS \"Message Dir\",sms_name AS \"SMS Name\",count(*) count from psms_log where MESSAGE_TIMESTAMP >= sysdate-20/1440 group by messg_dir, sms_name order by messg_dir,sms_name desc";
                break;
            case "smsStatsLast60Min":
                theQuery = "select messg_dir AS \"Message Dir\",sms_name AS \"SMS Name\",count(*) count from psms_log where MESSAGE_TIMESTAMP >= sysdate-60/1440 group by messg_dir, sms_name order by messg_dir,sms_name desc";
                break;
            case "OTAESNLookUp":
                theQuery = "SELECT OBJID, PART_SERIAL_NO \"Part Serial Number\", X_PART_INST_STATUS \"Part Inst Status\", X_DOMAIN \"Domain\", X_RED_CODE \"Red Code\", X_MSID MSID, X_PORT_IN \"Port in\",PART_INST2X_PERS \"Part Inst Pers\", PART_INST2X_NEW_PERS \"Part Inst New Pers\" FROM TABLE_PART_INST A WHERE PART_TO_ESN2PART_INST IN ( SELECT OBJID FROM TABLE_PART_INST WHERE PART_SERIAL_NO IN ('" + qParameter + "') )";
                break;
            case "OTACheckESNCallTrans":
                theQuery = "select OBJID, X_SERVICE_ID ESN, to_char(X_TRANSACT_DATE, 'MM-DD-YYYY HH24:MI') \"Transaction Date\", X_MIN MIN, X_RESULT RESULT from table_x_call_trans where x_service_id = '" + qParameter + "' order by x_service_id,x_transact_date desc";
                break;
            case "OTACheckESNTableTransaction":
                theQuery = "select to_char(x_transaction_date, 'MON-DD-YYYY HH24:MI:SS') day,x_status,x_min,x_action_type,x_mode, x_reason,x_carrier_code from table_x_ota_transaction where x_esn = '" + qParameter + "' order by x_transaction_date desc";
                break;
            case "OTACheckMINOTAPRD":
                theQuery = "select m365_id, to_char(message_timestamp, 'MON-DD-YYYY HH24:MI:SS') MSG_TIMESTAMP, SUBSCR_MDN, internal_trans_id \"INT TRANS ID\", call_trans_id \"CALL TRANS ID\", P_ACK, MESSG_DIR, PARSED_MSG, ORIGINAL_MSG, PIN_NUMBER, SOURCE_SYSTEM from PSMS_LOG a where a.subscr_mdn = (' " + qParameter + "') order by message_timestamp,ORIGINAL_MSG,a.subscr_mdn";
                break;

            // QUERIES FOR OTA PENDINGS
            case "OTACurrentActivationRedemption":
                theQuery = "select decode(x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode') \"Pending Transaction Type\", count(*) \"Count\" from table_x_ota_transaction where x_status = 'OTA PENDING' and x_action_type ||'' in ('1','3','6','7') and x_transaction_date > trunc(sysdate) group by x_action_type";
                break;
            case "OTACurrentActivationRedemptionSourceSystem":
                theQuery = "select decode(x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode') \"Pending Transaction Type\", x_mode \"SOURCE SYSTEM\", count(*) \"COUNT\" from table_x_ota_transaction where x_status = 'OTA PENDING' and x_action_type ||'' in ('1','3','6','7') and x_transaction_date > trunc(sysdate) group by x_action_type, x_mode";
                break;
            case "OTAPendingsPerPartClass":
                theQuery = "SELECT pc.name NAME,count(*) COUNT FROM table_part_inst pi,table_mod_level ml,table_part_num pn,table_x_ota_transaction ct,table_part_class pc WHERE ct.x_transaction_date > TRUNC (SYSDATE) and ct.x_action_type = '1' and ct.x_status = 'OTA PENDING' and ct.x_esn = pi.part_serial_no and pi.n_part_inst2part_mod = ml.objid and pn.part_num2part_class = pc.objid and ml.part_info2part_num = pn.objid group by pc.name order by count(*) desc";
                break;
            case "OTAPendingsPerPartNumber":
                theQuery = "SELECT pn.part_number PARTNUMBER,count(*) COUNT FROM table_part_inst pi,table_mod_level ml,table_part_num pn,table_x_ota_transaction ct WHERE ct.x_transaction_date > TRUNC (SYSDATE) and ct.x_action_type = '1' and ct.x_status = 'OTA PENDING' and ct.x_esn = pi.part_serial_no and pi.n_part_inst2part_mod = ml.objid and ml.part_info2part_num = pn.objid group by pn.part_number order by count(*) desc";
                break;
            case "SafelinkOTAPendings":
                theQuery = " select decode(ot.x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode') TYPE, count(*) \"Count\" from table_x_ota_transaction ot, table_x_call_trans ct where ot.x_status = 'OTA PENDING' and ot.x_action_type ||'' in ('1','3','6','7') and ot.x_transaction_date >= trunc(sysdate) and ot.x_transaction_date < trunc(sysdate+1) and ot.x_ota_trans2x_call_trans = ct.objid and (ct.x_call_trans2dealer = 1325962738 OR ot.x_esn in (select x_esn from x_program_enrolled where x_sourcesystem ='VMBC')) group by decode(ot.x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode')";
                break;
            case "OTAPendingsRefurbished":
                theQuery = "select decode(ot.x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode') TYPE, count(*) \"COUNT\" from table_x_ota_transaction ot, table_x_call_trans ct,table_site_part st where ot.x_status = 'OTA PENDING' and ot.x_action_type ||'' in ('1','3','6','7') and ot.x_transaction_date > trunc(sysdate-1) and ot.x_transaction_date <= trunc(sysdate+1) and ot.x_ota_trans2x_call_trans = ct.objid and st.x_refurb_flag = 1 and ct.call_trans2site_part = st.objid group by decode(ot.x_action_type,'1','Activation','3','Reactivation','6','Redemption','7','Persgencode')";
                break;

            // Queries for Parameters
            case "OTAParameters":
                theQuery = "select x_source_system \"Source System\",x_message_response \"Message Response\", to_char(x_start_date, 'MON-DD-YYYY HH24:MI:SS') \"Start Date\",x_redm_enabled \"Redm Enabled\","
                        + "x_act_enabled \"Act Enabled\",x_react_enabled \"React Enabled\", x_mo_enabled \"MO Enabled\",x_mt_enabled \"MT Enabled\","
                        + "x_Refill_training \"Refill Training\", x_refill_count \"Refill Count\", x_ild_counter \"ILD Counter\", x_max_feature_count"
                        + "\"Max Feature Count\",x_buy_airtime_enabled \"Buy Airtime Enabled\""
                        + " from SA.TABLE_X_OTA_PARAMS";
                break;
            case "LowBalanceParams":
                theQuery = "select objid,dev,x_param_name \"Param Name\",x_param_value \"Param Value\", x_notes \"Notes\" from table_X_parameters where x_param_name like 'LOW_BALANCE_%'";
                break;  
            case "TableXParams":
                theQuery = "select objid,dev,x_param_name \"Param Name\",x_param_value \"Param Value\",x_notes \"Notes\" from table_X_parameters";
                break;     
            case "ProgramXParams":
                theQuery = "select x_program_name \"Program Name\" from x_program_parameters";
                break; 
  
            // QUERIES FOR OTA PENDINGS CARRIER
            case "OTAPendingsCarrierActivations":
                theQuery = "select p.x_parent_name PARENTNAME,ct.x_action_text text,count(*) pending from table_x_parent p, table_x_carrier_group g, table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier ca where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('1') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = ca.objid and ca.carrier2carrier_group = g.objid and g.x_carrier_group2x_parent = p.objid and ot.X_STATUS = 'OTA PENDING' group by p.X_PARENT_NAME, ct.x_action_text order by count(*) desc";
                break;
            case "OTAPendingsCarrierReactivations":
                theQuery = "select p.x_parent_name CARRIER,ct.x_action_text text,count(*) pending from table_x_parent p, table_x_carrier_group g, table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier ca where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('3') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = ca.objid and ca.carrier2carrier_group = g.objid and g.x_carrier_group2x_parent = p.objid and ot.X_STATUS = 'OTA PENDING' group by p.X_PARENT_NAME, ct.x_action_text order by count(*) desc";
                break;
            case "OTAPendingsCarrierPersonality":
                theQuery = "select p.x_parent_name CARRIER,ct.x_action_text TYPE,count(*) pending from table_x_parent p, table_x_carrier_group g, table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier ca where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('7') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = ca.objid and ca.carrier2carrier_group = g.objid and g.x_carrier_group2x_parent = p.objid and ot.X_STATUS = 'OTA PENDING' group by p.X_PARENT_NAME, ct.x_action_text order by count(*) desc";
                break;
            case "OTAPendingsCarrierRedemptions":
                theQuery = "select p.x_parent_name CARRIER,ct.x_action_text TYPE,count(*) \"NUMBER\" from table_x_parent p, table_x_carrier_group g, table_x_carrier c, table_x_call_trans ct where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('6')and x_result ||'' = 'OTA PENDING' and ct.x_call_trans2carrier = c.objid and c.carrier2carrier_group = g.objid and g.x_carrier_group2x_parent = p.objid group by p.X_PARENT_NAME,ct.x_action_text order by count(*) desc";
                break;
            case "OTAPendingsCarrierActivationsDetails":
                theQuery = "select c.x_mkt_submkt_name carrier,ct.x_action_text text,ct.x_sourcesystem \"source system\",count(*) pending from table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier c where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('1') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = c.objid and ot.X_STATUS = 'OTA PENDING' group by c.x_mkt_submkt_name,ct.x_action_text,ct.x_sourcesystem order by count(*) desc";
                break;
            case "OTAPendingsCarrierReactivationsDetails":
                theQuery = "select c.x_mkt_submkt_name carrier,ct.x_action_text text,ct.x_sourcesystem \"source system\",count(*) pending from table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier c where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('3') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = c.objid and ot.X_STATUS = 'OTA PENDING' group by c.x_mkt_submkt_name,ct.x_action_text,ct.x_sourcesystem order by count(*) desc";
                break;
            case "OTAPendingsCarrierPersonalityDetails":
                theQuery = "select c.x_mkt_submkt_name carrier,ct.x_action_text text,ct.x_sourcesystem \"source system\",count(*) pending from table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier c where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('7') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = c.objid and ot.X_STATUS = 'OTA PENDING' group by c.x_mkt_submkt_name,ct.x_action_text,ct.x_sourcesystem order by count(*) desc";
                break;
            case "OTAPendingsCarrierRedemptionsDetails":
                theQuery = "select c.x_mkt_submkt_name carrier,ct.x_action_text text,ct.x_sourcesystem \"source system\",count(*) pending from table_x_call_trans ct, table_x_ota_transaction ot,table_x_carrier c where ct.x_transact_date >= trunc(sysdate) and ct.x_action_type ||'' in ('6') and ct.objid = ot.x_ota_trans2x_call_trans and ct.x_call_trans2carrier = c.objid and ot.X_STATUS = 'OTA PENDING' group by c.x_mkt_submkt_name,ct.x_action_text,ct.x_sourcesystem order by count(*) desc";
                break;

            // PAYMENT SERVICES
            case "creditCardTransactionCheck":
                theQuery = "select to_char(x_rqst_date, 'MON-DD-YYYY HH24:MI:SS') \"Date & Time\" ,x_ics_rmsg Message,x_ics_rcode Code from table_x_purch_hdr where x_esn='" + qParameter + "' and x_rqst_date >sysdate-1";
                break;
            case "paymentStatus":
                theQuery = "select X_PEND_PAY_ID ID, STATUS, MIN, MID, ESN, SOURCE_SYSTEM \"Source System\", PROMO_CODE \"Promo Code\", AMOUNT, to_char(RESENT_DATE, 'MON-DD-YYYY HH24:MI:SS') \"Date & Time\" from sa.x_pending_payment where resent_date>=(trunc(sysdate,'HH24')-1/24) and status not in ('DONE','FAILPAY','FAILGEN')";
                break;
            case "paymentDetails":
                theQuery = "select X_PEND_PAY_ID ID, STATUS, MIN, MID, ESN, SOURCE_SYSTEM Source, PROMO_CODE Promo, AMOUNT, to_char(RESENT_DATE, 'MON-DD-YYYY HH24:MI:SS') \"Date & Time\", RED_CARD_PIN PIN, DLL from sa.x_pending_payment where resent_date>trunc(sysdate,'HH')-4/24";
                break;
            case "gencodeDeliveryDetails":
                theQuery = "select to_char(Trans_Date, 'MON-DD-YYYY') AS \"Date\", Success_Rate Success, Failure_Rate AS \"Failure\", Cust_Redeemed_Rate AS \"Cust Redeemed\", Posted_Rate AS \"Posted\", Waiting_Rate AS \"Waiting\", Received_Rate AS \"Received\" from (\n" +
                        "(select trunc(x_insert_date) Trans_Date, (count(decode(X_STATUS, 'PROCESSED',1,null))) || ' ( ' || ROUND((count(decode(X_STATUS, 'PROCESSED',1,null))) /(count(*)) * 100 ,2)||'% )' Success_Rate, \n" +
                        "            (count(decode(X_STATUS, 'FAILED',1,null))) || ' ( ' || ROUND((count(decode(X_STATUS, 'FAILED',1,null))) /(count(*)) * 100,2)||'% )' Failure_Rate, (count(decode(X_STATUS, 'NOPENDING',1,null))) || ' \n" +
                        "            ( ' || ROUND((count(decode(X_STATUS, 'NOPENDING',1,null))) /(count(*)) * 100,2)||'% )' Cust_Redeemed_Rate, (count(decode(X_STATUS, 'POSTED',1,null))) || ' \n" +
                        "            ( ' || ROUND((count(decode(X_STATUS, 'POSTED',1,null))) /(count(*)) * 100,2)||'% )' Posted_Rate, (count(decode(X_STATUS, 'INSERTED',1,null))) || ' \n" +
                        "            ( ' || ROUND((count(decode(X_STATUS, 'INSERTED',1,null))) /(count(*)) * 100,2)||'% )' Waiting_Rate, (count(decode(X_STATUS, 'RECEIVED',1,null))) || ' \n" +
                        "            ( ' || ROUND((count(decode(X_STATUS, 'RECEIVED',1,null))) /(count(*)) * 100,2)||'% )' Received_Rate from x_program_gencode \n" +
                        "            where x_insert_date >= trunc(sysdate) and x_insert_date < trunc(sysdate)+1 group by trunc(x_insert_date)) order by Trans_Date)";
                break;
            case "gencodeDeliveryFailedDetails":
                theQuery = "select objid, X_ESN, to_char(x_insert_date, 'MON-DD-YYYY HH24:MI:SS') \"Insert Date\", X_POST_DATE AS \"Post Date\", X_ERROR_NUM AS \"Error Num\", X_ERROR_STRING AS \"Error String\", to_char(X_UPDATE_STAMP, 'MON-DD-YYYY HH24:MI:SS') \"Update\", GENCODE2PROG_PURCH_HDR \"Gencode2Prog HDR\", GENCODE2CALL_TRANS \"Gencode2Call Trans\" from x_program_gencode where x_status in ( 'FAILED') and x_insert_date >= trunc(sysdate) order by x_insert_date desc";
                break;
            case "enrollmentStatusValuePlanChannels":
                theQuery = "select x_sourcesystem SOURCE,count(*) COUNT from x_program_enrolled where x_insert_date >= trunc(sysdate) and x_insert_date <= trunc(sysdate)+1 group by x_sourcesystem";
                break;
            case "enrollmentStatusDetailsValuePlanChannels":
                theQuery = "select x_sourcesystem SOURCE, x_program_name \"PROGRAM NAME\", count(*) COUNT from x_program_enrolled pe, x_program_parameters pp where pe.pgm_enroll2pgm_parameter = pp.objid and x_enrolled_date >= trunc(sysdate) group by x_sourcesystem, x_program_name";
                break;
            case "billingPlatformBatchPaymentStatus":
                theQuery = "SELECT C.X_BATCH_ID \"Batch Id\", ROUND(( (A.X_END_TIME-A.X_START_TIME)*24*60), 2) MINUTES, A.X_STATUS FROM X_JOB_RUN_DETAILS A, X_JOB_MASTER B, X_PROGRAM_BATCH C WHERE a.RUN_DETAILS2JOB_MASTER = b.OBJID AND A.RUN_DETAILS2JOB_MASTER IN('1100009') AND TRUNC(A.X_START_TIME) = TRUNC(C.BATCH_SUB_DATE) AND A.X_START_TIME >= (SYSDATE-20) ORDER BY A.X_START_TIME DESC";
                break;
            case "billingPlatformBatchPaymentCount":
                theQuery = "select to_char(min(x_rqst_date), 'MON-DD-YYYY HH24:MI:SS') \"Start Date\", to_char(max(x_rqst_date), 'MON-DD-YYYY HH24:MI:SS') \"End Date\", count(decode(x_ics_rcode, 1, 1,NULL)) || ' (' ||ROUND((count(decode(x_ics_rcode, 1, 1,NULL))) / (count(*)) * 100, 2)||'% )' SUCCESS , count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,NULL,1)), NULL)) || ' (' ||ROUND((count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,NULL,1)), NULL))) / (count(*)) * 100, 2)||'% )' \"PAYMENTS DECLINED\", count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,1,NULL)), NULL)) || ' (' || ROUND((count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,1,NULL)), NULL))) / (count(*)) * 100, 2)||'% )' \"SYSTEM ERRORS\", count(*) Total from x_program_purch_hdr where x_rqst_date >= trunc(sysdate) and x_rqst_date < trunc(sysdate)+1";
                break;
            case "ccRate":
                theQuery = "select to_char(min(x_rqst_date), 'MON-DD-YYYY HH24:MI:SS') \"Start Date\", to_char(max(x_rqst_date), 'MON-DD-YYYY HH24:MI:SS') \"End Date\", count(decode(x_ics_rcode, 1, 1,NULL)) || ' (' ||ROUND((count(decode(x_ics_rcode, 1, 1,NULL))) / (count(*)) * 100, 2)||'% )' \"SUCCESS\", count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,NULL,1)), NULL)) || ' (' ||ROUND((count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,NULL,1)), NULL))) / (count(*)) * 100, 2)||'% )' \"PAYMENTS DECLINED\", count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,1,NULL)), NULL)) || ' (' || ROUND((count(decode(x_ics_rcode, 0, (decode(x_ics_rmsg,NULL,1,NULL)), NULL))) / (count(*)) * 100, 2)||'% )' \"SYSTEM ERRORS\", count(*) Total from x_program_purch_hdr where x_rqst_date >= trunc(sysdate) and x_rqst_date < trunc(sysdate)+1";
                break;
            case "billingPaymentTrans20Min":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) \"TOTAL PASSED\", COUNT(*) \"TOTAL ATTEMPTS\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 \"SUCCESS (%)\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 \"TOTAL (%)\" FROM x_program_purch_hdr WHERE X_RQST_DATE >= (sysdate-20/1440) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL and x_merchant_id is not null GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "billingPaymentTrans1Hour":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) \"TOTAL PASSED\", COUNT(*) \"TOTAL ATTEMPTS\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 \"SUCCESS (%)\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 \"TOTAL (%)\" FROM x_program_purch_hdr WHERE X_RQST_DATE >= (sysdate-1/24) AND x_rqst_date <= (sysdate) AND x_rqst_source IS NOT NULL and x_merchant_id is not null GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "billingPaymentTransAllDay":
                theQuery = "SELECT x_rqst_source SOURCE, x_merchant_id COMPANY, SUM(DECODE(x_ics_rcode,1,1,100,1,0)) \"TOTAL PASSED\", COUNT(*) \"TOTAL ATTEMPTS\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/COUNT(*),2) *100 \"SUCCESS (%)\", ROUND(SUM(DECODE(x_ics_rcode,1,1,100,1,0))/SUM(COUNT(*)) over(),2) *100 \"TOTAL (%)\" FROM x_program_purch_hdr WHERE X_RQST_DATE >= trunc(sysdate) AND x_rqst_date <= trunc(sysdate) + 1 AND x_rqst_source IS NOT NULL and x_merchant_id is not null GROUP BY x_rqst_source, x_merchant_id";
                break;
            case "esnApp":
                theQuery = "SELECT TO_CHAR(x_rqst_date,'MON-DD-YYYY HH24:MI:SS') \"Date\", x_esn, X_CUSTOMER_EMAIL \"Customer Email\", x_rqst_source \"Request Source\", x_rqst_type \"Request Type\", x_merchant_id \"Merchant ID\", x_ics_rcode \"ICR RCode\", x_ics_rflag \"ICS RFlag\", x_ics_rmsg \"ICS RMessage\" FROM X_program_purch_hdr WHERE x_rqst_date >= sysdate-240/1440 AND X_ESN ='" + qParameter + "' ORDER BY \"Date\"";
                break;
            case "esnBP":
                theQuery = "SELECT TO_CHAR(ph.X_RQST_DATE,'MON-DD-YYYY HH24:MI:SS') \"Date\", ph.x_esn, ph.X_CUSTOMER_EMAIL \"Customer Email\", ph.x_rqst_source \"Request Source\", ph.x_rqst_type \"Request Type\", ph.x_merchant_id \"Merchant ID\", ph.x_ics_rcode \"ICR RCode\", ph.x_ics_rflag \"ICS RFlag\", ph.x_ics_rmsg \"ICS RMessage\" from x_program_purch_hdr ph, x_program_purch_dtl pd, x_program_enrolled pe where ph.OBJID = pd.pgm_purch_dtl2prog_hdr AND pd.pgm_purch_dtl2pgm_enrolled = pe.objid and pe.x_esn in ( '" + qParameter + "' ) and x_bill_amount > 0 and x_rqst_date > (sysdate - 4/24)";
                break;
            case "emailSearch":
                theQuery = "SELECT TO_CHAR(x_rqst_date,'MON-DD-YYYY HH24:MI:SS') \"Date\", X_esn, X_CUSTOMER_EMAIL \"Customer Email\", x_rqst_source \"Source\", x_rqst_type \"Type\", x_merchant_id \"Merchant ID\", x_ics_rcode \"Code\", x_ics_rflag \"Flag\", x_ics_rmsg \"Message\" FROM X_program_purch_hdr WHERE x_rqst_date >= sysdate-240/1440 AND X_CUSTOMER_EMAIL ='" + qParameter + "' ORDER BY \"Date\"";
                break;

            // Queries for PCRF
            case "pcrfQueuesAutogen":
                theQuery = "select count(*) COUNT from x_pcrf_transaction where PCRF_STATUS_CODE='Q'";
                break;    
            case "pcrfUpdates1Hour":
                theQuery = "select pcrf_status_code CODE,ORDER_TYPE TYPE, count(*) COUNT from x_pcrf_transaction where update_timestamp >= sysdate - 1/24 group by pcrf_status_code,ORDER_TYPE ";
                break;
            case "pcrfUpdates30Min":
                theQuery = "select pcrf_status_code CODE,ORDER_TYPE TYPE, count(*) COUNT from x_pcrf_transaction where update_timestamp >= sysdate - 30/1440 group by pcrf_status_code,ORDER_TYPE";
                break;
            case "pcrfUpdates5Min":
                theQuery = "select pcrf_status_code CODE,ORDER_TYPE TYPE, count(*) COUNT from x_pcrf_transaction where update_timestamp >= sysdate - 5/1440 group by pcrf_status_code,ORDER_TYPE";
                break;
                
            // Queries for Phone Activation
            case "programmed":
                theQuery = "select to_char(x_transact_date,'MON-DD-YYYY') \"Date\",count(*) \"Count\" from table_x_call_trans \n" +
                            "where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' AND x_action_type IN ('1','3') and \n" +
                            "not exists (select 1 from sa.x_dummy_data where x_esn = table_x_call_trans.x_service_id) group by to_char(x_transact_date,'MON-DD-YYYY')";
                break;
            case "programmedByHour":
                theQuery = "select to_char(x_transact_date,'MON-DD-YYYY    HH24') \"Date\",count(*) \"Count\" from table_x_call_trans \n" +
                            "where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' AND x_action_type IN ('1','3') and not exists \n" +
                            "(select 1 from sa.x_dummy_data where x_esn = table_x_call_trans.x_service_id) group by to_char(x_transact_date,'MON-DD-YYYY    HH24') order by \"Date\"";
                break;
            case "programmedByHourWeb":
                theQuery = "SELECT x_sourcesystem,x_sub_sourcesystem,TO_CHAR(x_transact_date,'MON-DD-YYYY    HH24') \"Date\",COUNT(*) \"Count\" FROM table_x_call_trans \n" +
                            "WHERE x_transact_date >= TRUNC(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))AND x_transact_date < TRUNC(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 AND upper(x_result)= 'COMPLETED' AND x_action_type IN ('1','3') AND x_sourcesystem in ('WEB','UDP') AND NOT EXISTS\n" +
                            "(SELECT 1 FROM sa.x_dummy_data WHERE x_esn = table_x_call_trans.x_service_id ) GROUP BY x_sourcesystem,x_sub_sourcesystem,TO_CHAR(x_transact_date,'MON-DD-YYYY    HH24') ORDER BY \"Date\",x_sourcesystem,x_sub_sourcesystem";
                break;
            case "programmedByHourIVR":
                theQuery = "SELECT x_sourcesystem,x_sub_sourcesystem,TO_CHAR(x_transact_date,'MON-DD-YYYY    HH24') \"Date\",COUNT(*) \"Count\" FROM table_x_call_trans \n" +
                            "WHERE x_transact_date >= TRUNC(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))AND x_transact_date < TRUNC(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 AND upper(x_result)= 'COMPLETED' AND x_action_type IN ('1','3') AND x_sourcesystem in ('IVR') AND NOT EXISTS\n" +
                            "(SELECT 1 FROM sa.x_dummy_data WHERE x_esn = table_x_call_trans.x_service_id ) GROUP BY x_sourcesystem,x_sub_sourcesystem,TO_CHAR(x_transact_date,'MON-DD-YYYY    HH24') ORDER BY \"Date\",x_sourcesystem,x_sub_sourcesystem";
                break;
            case "programmedByHourBrand":
                theQuery = "select to_char(x_transact_date,'MON-DD-YYYY    HH24') \"Date\",x_sub_sourcesystem Brand ,count(*) amount from table_x_call_trans \n" +
                            "where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' AND x_action_type IN ('1','3') and not exists \n" +
                            "(select 1 from sa.x_dummy_data where x_esn = table_x_call_trans.x_service_id) group by to_char(x_transact_date,'MON-DD-YYYY    HH24'), x_sub_sourcesystem order by \"Date\"";
                break;
            case "cardsRedemmed":
                theQuery = "select to_char(x_red_date,'MON-DD-YYYY') \"Date\",count(*) Amount from table_x_red_card \n" +
                            "where x_result||'' = 'Completed' and x_red_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_red_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 group by to_char(x_red_date,'MON-DD-YYYY')";
                break;
            case "cardsRedemmedByHour":
                theQuery = "select to_char(x_red_date,'MON-DD-YYYY HH24') \"Date\",count(*) Amount from table_x_red_card \n" +
                            "where x_result||'' = 'Completed' and x_red_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_red_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 group by to_char(x_red_date,'MON-DD-YYYY HH24') order by \"Date\"";
                break;
            case "cardsRedemmedByHourBrand":
                theQuery = " select to_char(red.x_red_date,'MON-DD-YYYY HH24') \"Date\", ct.x_sub_sourcesystem Brand, count(*) Amount from table_x_red_card red, table_x_call_trans ct \n" +
                            " where 1=1 and red.x_result||'' = 'Completed' and red.x_red_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and red.x_red_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and red.red_card2call_trans = ct.objid \n" +
                            " group by to_char(red.x_red_date,'MON-DD-YYYY HH24'),x_sub_sourcesystem order by \"Date\"";
                break;
            case "activationsSent2Carrier":
                theQuery = " select to_char(trans_date,'MON-DD-YYYY') \"Date\", total, total_closed \"Total Closed\", success_rate \"Success Rate (%)\" from (select a.trans_date, a.total, nvl(b.total,0) total_closed, substr((nvl(b.total,0)/a.total)*100,1,4) success_rate \n" +
                            " from (select trunc(ct.x_transact_date) TRANS_DATE, count(*) total from table_x_call_trans ct,table_task t \n" +
                            " where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and ct.x_action_type in ('1', '3') and ct.objid=t.x_task2x_call_trans and ct.x_result = 'Completed' \n" +
                            " group by trunc(ct.x_transact_date)) a, (select trunc(ct.x_transact_date) TRANS_DATE, count(*) total from table_x_call_trans ct,table_task t,table_condition c \n" +
                            " where ct.x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and ct.x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' and ct.x_action_type in ('1', '3') \n" +
                            " and ct.objid=t.x_task2x_call_trans and t.TASK_STATE2CONDITION=c.objid and c.condition||'' = 8192 \n" +
                            " group by trunc(ct.x_transact_date)) b where 1=1 and a.trans_date = b.trans_date(+))";
                break;
            case "activationsByBrand":
                theQuery = " select to_char(x_transact_date,'MON-DD-YYYY HH24') \"Date\",x_sub_sourcesystem Brand ,count(*) amount \n" +
                            " from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' AND x_action_type = '1' and not exists \n" +
                            " (select 1 from sa.x_dummy_data where x_esn = table_x_call_trans.x_service_id) group by to_char(x_transact_date,'MON-DD-YYYY HH24'), x_sub_sourcesystem order by \"Date\"";
                break;
            case "reactivationsByBrand":
                theQuery = " select to_char(x_transact_date,'MON-DD-YYYY HH24') \"Date\",x_sub_sourcesystem Brand ,count(*) amount \n" +
                            " from table_x_call_trans where x_transact_date >= trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD')) and x_transact_date < trunc(TO_DATE('" + qParameter + "', 'YYYY-MM-DD'))+1 and upper(x_result) = 'COMPLETED' AND x_action_type = '3' and not exists \n" +
                            " (select 1 from sa.x_dummy_data where x_esn = table_x_call_trans.x_service_id) group by to_char(x_transact_date,'MON-DD-YYYY HH24'), x_sub_sourcesystem order by \"Date\"";
                break;
            case "stQueuedCardsCheck":
                theQuery = "SELECT TO_CHAR(x_transact_date,'HH24') AS hour, COUNT(*) Cards \n" +
                            " FROM table_x_call_trans \n" +
                            " WHERE x_reason = 'Queue_Card_Delivery' AND x_sub_sourcesystem = 'STRAIGHT_TALK' AND x_sourcesystem = 'BATCH' \n" +
                            " AND x_transact_date >= TRUNC(sysdate) AND X_RESULT ='Completed' GROUP BY TO_CHAR(x_transact_date,'HH24') ORDER BY TO_CHAR(x_transact_date,'HH24')";
                break;
            case "ivrMinChangeUpgradeStatus":
                theQuery = " SELECT TO_CHAR(creation_date,'MON-DD-YYYY') \"Date\", status, COUNT(*) \"Count\" \n" +
                            " FROM sa.TABLE_QUEUED_CBO_SERVICE WHERE creation_date >= TRUNC(sysdate)-1 \n" +
                            " GROUP BY TO_CHAR(creation_date,'MON-DD-YYYY'), status ORDER BY TO_CHAR(creation_date,'MON-DD-YYYY'), status";
                break;
            case "portsToday":
                theQuery = "SELECT c.customer_code CHANNEL, c.case_type_lvl2 BRAND, count(*) \"Count\" FROM table_case c, table_x_case_detail cd, table_x_case_detail cd1 WHERE cd.detail2case = c.objid AND cd1.detail2case = c.objid AND c.x_case_type = 'Port In' AND upper(c.title) LIKE '%EXTERNAL%' AND cd.x_name = 'CURRENT_MIN' AND C.creation_time >= TRUNC(sysdate) AND c.creation_time < TRUNC(sysdate) +1 AND cd1.x_name = 'ASSIGNED_CARRIER' AND EXISTS (SELECT 1 FROM table_x_case_detail cd WHERE cd.detail2case = c.objid AND cd.x_name LIKE '%CURR_ADDR%' ) GROUP BY c.customer_code, c.case_type_lvl2";
                break;

            // Queries for POSA
            case "cardSwipe":
                theQuery = "select vendor, sum(swipe) swipe, sum(unswipe) unswipe, sum(returned) returned from \n" +
                            "(select p.tf_serial_num, c.name vendor, nvl(max(decode(p.toss_posa_action,'SWIPE',1)),0) swipe, \n" +
                            "nvl(max(decode(p.toss_posa_action,'UNSWIPE',1)),0) unswipe, \n" +
                            "nvl(max(decode(p.toss_posa_action,'RETURNED',1)),0) returned \n" +
                            "from table_site c, x_posa_card p \n" +
                            "where p.toss_posa_date>trunc(sysdate) and \n" +
                            "p.toss_posa_date<trunc(sysdate+1) \n" +
                            "and c.site_id=p.toss_site_id group by p.tf_serial_num, c.name) \n" +
                            "group by vendor order by 2 desc";
                break;
            case "phoneSwipe":
                theQuery = "select vendor, sum(swipe) swipe, sum(unswipe) unswipe, sum(returned) returned from \n" +
                            "(select p.tf_serial_num, c.name vendor, nvl(max(decode(p.toss_posa_action,'SWIPE',1)),0) swipe, \n" +
                            "nvl(max(decode(p.toss_posa_action,'UNSWIPE',1)),0) unswipe, \n" +
                            "nvl(max(decode(p.toss_posa_action,'RETURNED',1)),0) returned \n" +
                            "from table_site c, x_posa_phone p \n" +
                            "where p.toss_posa_date>trunc(sysdate) and \n" +
                            "p.toss_posa_date<trunc(sysdate+1) \n" +
                            "and c.site_id=p.toss_site_id group by p.tf_serial_num, c.name) \n" +
                            "group by vendor order by 2 desc";
                break;
            case "cardSwipeBHN":
                theQuery = "select to_char(toss_posa_date,'HH24:MI') TIME ,count(*) COUNT from sa.x_posa_card where 1=1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 and toss_posa_action='SWIPE' and sourcesystem='BHN' group by to_char(toss_posa_date, 'HH24:MI') order by to_char(toss_posa_date,'HH24:MI') desc";
                break;
            case "phoneSwipeBHN":
                theQuery = "select to_char(toss_posa_date,'HH24:MI') TIME ,count(*) COUNT from sa.x_posa_phone where 1=1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 and toss_posa_action='SWIPE' and sourcesystem='BHN' group by to_char(toss_posa_date, 'HH24:MI') order by to_char(toss_posa_date,'HH24:MI') desc";
                break;
            case "incommCards":
                theQuery = "select to_char(toss_posa_date,'HH24:MI') TIME ,count(*) COUNT from sa.x_posa_card where 1=1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 and toss_posa_action='SWIPE' and sourcesystem='POSA' group by to_char(toss_posa_date, 'HH24:MI') order by to_char(toss_posa_date,'HH24:MI') desc";
                break;
            case "incommPhones":
                theQuery = "select to_char(toss_posa_date,'HH24:MI') TIME ,count(*) COUNT from sa.x_posa_phone where 1=1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 and toss_posa_action='SWIPE' and sourcesystem='POSA' group by to_char(toss_posa_date, 'HH24:MI') order by to_char(toss_posa_date,'HH24:MI') desc";
                break;
            case "POSAMonitorCheck":
                theQuery = "select to_char(SYSDATE,'MON-DD-YYYY HH24:MI') \"Date & Time\",TAB1.SALE AS SALES,TAB2.RETURNS AS RETURNS FROM (select count(*) SALE from x_posa_card where toss_posa_date >= trunc(SYSDATE) and toss_posa_date < trunc(sysdate)+1 and toss_posa_action = 'SWIPE' group by trunc(toss_posa_date)) TAB1, (select count(*) RETURNS from x_posa_card where toss_posa_date >= trunc(SYSDATE) and toss_posa_date < trunc(sysdate)+1 and toss_posa_action in ('UNSWIPE','RETURNED') group by trunc(toss_posa_date)) TAB2";
                break;
            case "cardSwipeBreakdown":
                theQuery = "select to_char(toss_posa_date,'HH24:MI') TIME ,count(*) COUNT from sa.x_posa_card where 1=1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 and toss_posa_action='SWIPE' group by to_char(toss_posa_date, 'HH24:MI') order by to_char(toss_posa_date,'HH24:MI') desc";
                break;
            case "posaActionCards":
                theQuery = "SELECT sourcesystem AS \"Source System\", toss_posa_action AS Action, to_char(TOSS_POSA_DATE,'HH24') t_hour, COUNT(*) COUNT FROM sa.x_posa_card WHERE 1 =1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 group by sourcesystem, toss_posa_action, to_char(TOSS_POSA_DATE,'HH24') order by t_hour desc, sourcesystem, toss_posa_action desc";
                break;
            case "posaActionPhones":
                theQuery = "SELECT sourcesystem AS \"Source System\", toss_posa_action AS Action, to_char(TOSS_POSA_DATE,'HH24') t_hour, COUNT(*) COUNT FROM sa.x_posa_phone WHERE 1 =1 AND toss_posa_date >= TRUNC(sysdate) AND toss_posa_date < TRUNC(sysdate) +1 group by sourcesystem, toss_posa_action, to_char(TOSS_POSA_DATE,'HH24') order by t_hour desc, sourcesystem, toss_posa_action desc";
                break;
            case "rtrStatus400":
                theQuery = "SELECT TO_CHAR(tf_trans_date,'MON-DD-YYYY    HH24') \"Date & Time\", rtr_vendor_name MERCHANT, COUNT(*) \"Count\" FROM X_Rtr_Trans WHERE 1=1 and tf_pin_status_code='400' AND tf_trans_date >= TRUNC(sysdate) AND tf_trans_Date < TRUNC(sysdate) +1 GROUP BY TO_CHAR(tf_trans_date,'MON-DD-YYYY    HH24'), rtr_vendor_name order BY TO_CHAR(tf_trans_date,'MON-DD-YYYY    HH24') desc, rtr_vendor_name desc";
                break;

            // Queries for RRP
            case "rrpBatchID":
                theQuery = "select X_BATCH_ID \"Batch ID\", BATCH_STATUS \"Batch Status\", TO_CHAR(BATCH_SUB_DATE,'MON-DD-YYYY HH24:MM:SS') \"Submitted\", TO_CHAR(BATCH_REC_DATE,'MON-DD-YYYY HH24:MM:SS') \"Received\", OBJID, PAYMENT_BATCH2X_CC_PARMS \"CC Params\" from X_Program_Batch where trunc(Batch_Sub_Date) > trunc(sysdate) -1 order by X_BATCH_ID desc";
                break;
            case "rrpBatchStatus":
                theQuery = "Select prog_hdr2prog_batch \"Batch ID\", x_status, count(*) as \"Count\" from x_program_purch_hdr hdr WHERE prog_hdr2prog_batch = '" + qParameter + "' group by x_status,prog_hdr2prog_batch";
                break;
            case "transactionsPerBatch":
                theQuery = "select prog_hdr2prog_batch \"Batch ID\", count(*) as COUNT from x_program_purch_hdr hdr WHERE prog_hdr2prog_batch ='" + qParameter + "' group by prog_hdr2prog_batch";
                break;
            case "failedByProductCode":
                theQuery = "select prog_hdr2prog_batch \"Batch ID\", X_PRODUCT_CODE \"Product Code\", count(*) as COUNT from x_program_purch_hdr hdr WHERE prog_hdr2prog_batch ='" + qParameter + "' and X_STATUS = 'FAILED' group by X_PRODUCT_CODE,prog_hdr2prog_batch";
                break;
            case "failedByCCCode":
                theQuery = "select PROG_HDR2PROG_BATCH \"Batch ID\", X_ICS_RCODE \"ICS Code\",COUNT(*) as COUNT, X_PRODUCT_CODE \"Product Code\",BCODE.X_CODE_DESC \"Description\" FROM X_PROGRAM_PURCH_HDR HDR , X_BILLING_CODE_TABLE BCODE WHERE HDR.PROG_HDR2PROG_BATCH ='" + qParameter + "' AND HDR.X_STATUS = 'FAILED' AND BCODE.X_CODE = X_ICS_RCODE AND X_CODE_TYPE='CC_REASON_CODES' GROUP BY PROG_HDR2PROG_BATCH, X_ICS_RCODE, X_PRODUCT_CODE,BCODE.X_CODE_DESC";
                break;

            // Queries for Safelink
            case "shipmentStatus":
                theQuery = "select count(*) COUNT, p.x_status STATUS, TO_CHAR(max(p.x_date_process),'MON-DD-YYYY HH24:MM:SS') \"PROCESS DATE\", TO_CHAR(max(a.creation_time),'MON-DD-YYYY HH24:MM:SS') \"CREATION DATE\", p.x_ff_center \"FF Center\" from table_case a, table_x_part_request p where creation_time > trunc(sysdate)-1 and creation_time < trunc(sysdate) and s_title||'' = 'LIFELINE SHIPMENT' and a.objid = p.request2case (+) group by x_status, p.x_ff_center order by TO_CHAR(max(a.creation_time),'MON-DD-YYYY HH24:MM:SS') asc";
                break;
            case "dailyStatus":
                theQuery = "select UPPER(X_ACTION_TEXT) TYPE, count(*) COUNT from table_x_call_trans ct where x_transact_date > trunc(sysdate) and x_call_trans2dealer = 1325962738 and x_action_type in ('1','3','6') GROUP BY UPPER(X_ACTION_TEXT) order by TYPE asc";
                break;

            // Queries for SmartPay
            case "smartPayOrdersLast10Days":
                theQuery = " select to_char(trunc(x_rqst_date), 'MON-DD-YYYY') \"REQUEST DATE\",channel,X_RQST_TYPE \"Request Type\",x_ics_applications \"ICS App\",order_type \"Order type\",x_ics_rmsg Message,count(*) \"Count\" from x_biz_purch_hdr bph, sa.TABLE_X_ALTPYMTSOURCE alt where bph.PURCH_HDR2ALTPYMTSOURCE=alt.objid and x_rqst_date >sysdate -10 and x_rqst_source='WEB' and X_RQST_TYPE='ALTSOURCE_PURCH' and alt.x_ALT_PYMT_SOURCE_TYPE='FINANCE' group by trunc(x_rqst_date),channel,X_RQST_TYPE, BPH.X_ICS_APPLICATIONS ,order_type,x_ics_rmsg";
                break;

            // Queries for SmartPay
            case "throttling":
                theQuery = "select ct.x_action_text ACTION, P.X_PARENT_NAME PARENT, ot.x_status STATUS,count(*) COUNT from table_x_ota_transaction ot, table_x_call_trans ct, sa.table_x_carrier xc, TABLE_X_PARENT P, TABLE_X_CARRIER_GROUP G where OT.X_OTA_TRANS2X_CALL_TRANS = ct.objid and CT.X_CALL_TRANS2CARRIER = xc.objid and ot.x_transaction_date >= trunc(sysdate) and ct.x_transact_date >= trunc(sysdate) and ct.x_action_type in ('1','3','6') and XC.CARRIER2CARRIER_GROUP = G.OBJID and G.X_CARRIER_GROUP2X_PARENT = P.OBJID and OT.X_STATUS in ('OTA PENDING','Completed') group by ct.x_action_text,P.X_PARENT_NAME, ot.x_status";
                break;

            // Queries for Tracfone-Safelink
            case "activationsReactivationsTransferRates":
                theQuery = "SELECT TO_CHAR(xfer_date, 'MON-DD-YYYY HH24') \"Date & Hour\", SUM (CASE WHEN (( application = '3' AND dnis IN ('1122', '1077', '2026','2131')) OR (application IN ('1', '2'))) THEN 1 ELSE 0 END) \"Act/React Calls\",SUM (CASE WHEN ( ( application = '3' AND dnis IN ('1122', '1077', '2026', '2131')) OR (application IN ('1', '2'))) AND ( disc_type LIKE 'XA%' OR disc_type IN ('BE', 'ME', 'MR', 'MC', 'MP', 'MN', 'OP')) THEN 1 ELSE 0 END) \"Act/React Transfers\",TO_CHAR ( ROUND ( SUM (CASE WHEN ( ( application = '3' AND dnis IN ('1122', '1077', '2026', '2131')) OR (application IN ('1', '2'))) AND ( disc_type LIKE 'XA%' OR disc_type IN ('BE','ME','MR','MC','MP','MN','OP')) THEN 1 ELSE 0 END) / SUM (CASE WHEN ( ( application = '3' AND dnis IN ('1122', '1077', '2026', '2131' ) )OR (application IN ('1', '2'))) THEN 1 ELSE 0 END), 3) * 100) || '%' \"Act/React Transfer Rate\" FROM ivr_report.tcr_logging_7days WHERE xfer_date > TRUNC (SYSDATE) + 8 / 24 AND company IN ('TF', 'SL')GROUP BY TO_CHAR (xfer_date, 'MON-DD-YYYY HH24') HAVING SUM (CASE WHEN ( ( application = '3' AND dnis IN ('1122','1077','2026','2131')) OR (application IN ('1', '2'))) THEN 1 ELSE 0 END) > 0 order by \"Date & Hour\" desc";
                break;
            case "refillTransferRates":
                theQuery = "SELECT TO_CHAR(xfer_date, 'MON-DD-YYYY HH24') \"Date & Hour\",SUM (CASE WHEN ( ( application = '3' AND dnis NOT IN ('1122','1077','2026','2131')) OR (application IN ('4', '5'))) THEN 1 ELSE 0 END) \"Refill Calls\", SUM (CASE WHEN ( ( application = '3' AND dnis NOT IN ('1122','1077','2026','2131')) OR (application IN ('4', '5'))) AND ( disc_type LIKE 'XA%' OR disc_type IN ('BE','ME','MR','MC','MP','MN','OP')) THEN 1 ELSE 0 END) \"Refill Transfers\", TO_CHAR ( ROUND ( SUM ( CASE WHEN ( ( application = '3' AND dnis NOT IN ('1122','1077','2026','2131')) OR (application IN ('4', '5'))) AND ( disc_type LIKE 'XA%' OR disc_type IN ('BE','ME','MR','MC','MP','MN','OP')) THEN 1 ELSE 0 END) / SUM (CASE WHEN ( ( application = '3' AND dnis NOT IN ('1122','1077','2026','2131')) OR (application IN ('4', '5'))) THEN 1 ELSE 0 END), 3) * 100) || '%' \"Refill Transfer Rate\" FROM ivr_report.tcr_logging_7days WHERE xfer_date > TRUNC (SYSDATE) + 8 / 24 AND company IN ('TF', 'SL') GROUP BY TO_CHAR (xfer_date, 'MON-DD-YYYY HH24') HAVING SUM (CASE WHEN ( ( application = '3' AND dnis NOT IN ('1122','1077','2026','2131')) OR (application IN ('4', '5'))) THEN 1 ELSE 0 END) > 0 order by \"Date & Hour\" desc";
                break;

            // Queries for WEX
            case "wexOpenOrders":
                theQuery = "select new_model \"New Model\", TO_CHAR(TO_DATE(order_date, 'MM/DD/YY'), 'MON-DD-YYYY') AS \"Order Date\",warehouse,FF_order_no \"FF Order Number\",TF_order_no \"Tracfone Order Number\", TO_CHAR(report_date, 'MON-DD-YYYY') \"Report Date\" from etladmin.open_orders where ID_number = '" + qParameter + "'";
                break;

            // Queries for WFM
            case "pinTransAllDay":
                theQuery = "select order_status \"Order Status\",to_char(po.INSERT_TIMESTAMP,'HH24') as Hour, count(*) as count from sa.x_process_order po, sa.x_process_order_detail pd where pd.process_order_objid = po.objid and external_order_id is null and brm_trans_id is null and po.INSERT_TIMESTAMP > trunc(sysdate) group by to_char(po.INSERT_TIMESTAMP,'HH24'),order_status";
                break;
            case "fiservTransAllDay":
                theQuery = "select order_status \"Order Status\",to_char(po.INSERT_TIMESTAMP,'HH24') as Hour, count(*) as count from sa.x_process_order po, sa.x_process_order_detail pd where pd.process_order_objid = po.objid and external_order_id is not null and po.INSERT_TIMESTAMP > trunc(sysdate) group by to_char(po.INSERT_TIMESTAMP,'HH24'),order_status";
                break;
            case "nonFiservNonPinTransAllDay":
                theQuery = "select order_status \"Order Status\",to_char(po.INSERT_TIMESTAMP,'HH24') as Hour, count(*) as count from sa.x_process_order po, sa.x_process_order_detail pd where pd.process_order_objid = po.objid and external_order_id is null and po.INSERT_TIMESTAMP > trunc(sysdate) group by to_char(po.INSERT_TIMESTAMP,'HH24'),order_status";
                break;

            // Queries for Sales
            case "salesOrigESN":
                theQuery = "Select distinct sh.BP_ORDER_NUM BP_ORDER, sh.PO_NUM TF_PO, sh.SHIP_TO CUSTOMER_NAME, to_char(sh.SHIP_DATE,'MM/DD/YYYY') " +
                            "SHIP_DATE,sh.SHIP_ADDRESS1 SHIP_ADDRESS,sh.SHIP_CITY CITY,sh.CC_PHONE PHONE, sh.EMAIL_ADDRESS From DBIT_RVURIMI.X_DS_SHIPMENTS sh, " +
                            "DBIT_RVURIMI.X_DS_SETTLEMENTS st Where st.FIELD1 = sh.BP_ORDER_NUM and sh.X_ESN = '" + qParameter + "' order by sh.BP_ORDER_NUM, " +
                            "sh.SHIP_CITY";
                break;
            case "salesRepESN":
                theQuery = "select * from (select (select sh.BP_ORDER_NUM from  DBIT_RVURIMI.X_DS_SHIPMENTS sh where M.ORIGINAL_ESN = sh.X_ESN and rownum <=1) " +
                            "BP_ORDER,M.REPLACEMENT_ESN, M.ORIGINAL_ESN,M.CASE_TITLE,M.id_number,M.CASE_CREATION_DATE From (select replace (tc.x_esn,'R','') " +
                            "ORIGINAL_ESN,pr.X_PART_SERIAL_NO REPLACEMENT_ESN,tc.id_number, tc.TITLE CASE_TITLE,tc.CREATION_TIME CASE_CREATION_DATE " +
                            "from sa.TABLE_CASE tc, sa.TABLE_X_PART_REQUEST pr where pr.REQUEST2CASE = tc.OBJID " +
                            "and pr.X_PART_SERIAL_NO in '" + qParameter + "') M)N where N.BP_ORDER Is Not Null";
                break;
            case "salesSName":
                theQuery = "select distinct sh.BP_ORDER_NUM BP_ORDER, sh.PO_NUM TF_PO, sh.SHIP_TO CUSTOMER_NAME, to_char(sh.SHIP_DATE,'MM/DD/YYYY') " +
                            "SHIP_DATE,sh.SHIP_ADDRESS1 SHIP_ADDRESS,sh.SHIP_CITY CITY,sh.CC_PHONE PHONE, sh.EMAIL_ADDRESS From DBIT_RVURIMI.X_DS_SHIPMENTS sh, " +
                            "DBIT_RVURIMI.X_DS_SETTLEMENTS st Where st.FIELD1 = sh.BP_ORDER_NUM and upper(sh.SHIP_TO) = '" + qParameter + "' order by sh.BP_ORDER_NUM, sh.SHIP_CITY";
                break;
            case "salesBPO":
                theQuery = "Select distinct sh.BP_ORDER_NUM BP_ORDER,sh.SHIP_TO CUSTOMER_NAME,to_char(sh.ORDER_DATE,'MM/DD/YYYY') " +
                            "ORDER_DATE, to_char(sh.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,sh.SHIP_ADDRESS1 SHIP_ADDRESS,sh.SHIP_CITY CITY,sh.CC_PHONE " +
                            "PHONE,sh.EMAIL_ADDRESS From DBIT_RVURIMI.X_DS_SHIPMENTS sh, DBIT_RVURIMI.X_DS_SETTLEMENTS st " +
                            "Where 1=1 and st.FIELD1 = sh.BP_ORDER_NUM and sh.BP_ORDER_NUM= '" + qParameter + "'";
                break;
            case "salesCustomerInfo":
                theQuery = "Select distinct sh.BP_ORDER_NUM BP_ORDER,sh.SHIP_TO CUSTOMER_NAME,to_char(sh.ORDER_DATE,'MM/DD/YYYY') " +
                            "ORDER_DATE, to_char(sh.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,sh.SHIP_ADDRESS1 SHIP_ADDRESS,sh.SHIP_CITY CITY,sh.CC_PHONE " +
                            "PHONE,sh.EMAIL_ADDRESS From DBIT_RVURIMI.X_DS_SHIPMENTS sh, DBIT_RVURIMI.X_DS_SETTLEMENTS st " +
                            "Where st.FIELD1 = sh.BP_ORDER_NUM and sh.BP_ORDER_NUM= '" + qParameter + "'";
                break;
            case "salesShippingInfo":
                theQuery = "select N.BP_ORDER, N.CUSTOMER_NAME, N.SHIP_DATE, N.X_ESN, N.CLASS, N.DESCRIPTION, N.TRACKING_NUM,N.SALES_AMOUNT, N.FREIGHT_AMOUNT, N.TAX_AMOUNT " +
                            "From (select distinct sh.X_ESN,sh.BP_ORDER_NUM BP_ORDER,to_char(sh.SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,sh.SHIP_TO " +
                            "CUSTOMER_NAME,sh.CLASS,sh.DESCRIPTION, sh.SHIP_ADDRESS1 SHIP_ADDRESS,sh.SHIP_CITY CITY,sh.CC_PHONE PHONE,sh.SALES_AMOUNT,sh.FREIGHT_AMOUNT,sh.TAX_AMOUNT, " +
                            "sh.TRACKING_NUM from DBIT_RVURIMI.X_DS_SHIPMENTS sh, DBIT_RVURIMI.X_DS_SHIPMENTS sh1 where sh1.BP_ORDER_NUM = sh.BP_ORDER_NUM " +
                            "and sh.BP_ORDER_NUM= '" + qParameter + "' order by sh.SALES_AMOUNT, sh.TAX_AMOUNT, sh.FREIGHT_AMOUNT) N";
                break;
            case "salesBillingRefund":
                theQuery = "select * from (select st1.FIELD1 BP_ORDER, to_char(st.REQUESTDATE,'MM/DD/YYYY') TRANS_DATE, st.TRANS_REF_NO TRANSACTION_, st.AMOUNT " +
                            "TRANS_AMOUNT, case when st.AMOUNT < 0 then 'REFUND' ELSE 'PAYMENT' END TRANS_TYPE from DBIT_RVURIMI.X_DS_SETTLEMENTS st, DBIT_RVURIMI.X_DS_SETTLEMENTS " +
                            "st1 where 1=1 and st.MERCHANTREFERENCENUMBER = st1.MERCHANTREFERENCENUMBER and st.TRANS_REF_NO is not null and st1.amount > '0' and st1.field1 in " +
                            "('" + qParameter + "') and rownum <= 1000 union select z.BP_ORDER, to_char(cb.CHARGEBACK_DATE,'MM/DD/YYYY'),cb.MERCHANT_ORDER, cb.ISSUER_CHARGEBACK_AMOUNT, " +
                            "cb.CB_TYPE TRANS_TYPE from SSADM.CB_MASTER cb, (select st1.FIELD1 BP_ORDER, to_char(st.REQUESTDATE,'MM/DD/YYYY') PURCHASE_DATE, " +
                            "st.TRANS_REF_NO TRANSACTION_, st.AMOUNT TRANS_AMOUNT, case when st.AMOUNT < 0 then 'REFUND' ELSE 'PAYMENT' END TRANS_TYPE " +
                            "from DBIT_RVURIMI.X_DS_SETTLEMENTS st, DBIT_RVURIMI.X_DS_SETTLEMENTS st1 where 1=1 and st.MERCHANTREFERENCENUMBER = st1.MERCHANTREFERENCENUMBER " +
                            "and st.TRANS_REF_NO is not null and st1.AMOUNT > '0' and st1.FIELD1 in ('" + qParameter + "') order by st.REQUESTDATE asc) z WHERE  z.TRANS_TYPE = 'PAYMENT' " +
                            "and cb.merchant_order = z.TRANSACTION_ and rownum <= 1000 ) R order by R.TRANS_DATE asc";
                break;
            case "salesBrightPoint":
                theQuery = "select sh.BP_ORDER_NUM BP_ORDER,to_char(di.RCPT_DATE,'MM/DD/YYYY') RCPT_DATE,di.SERIAL_NO RETURNED_ESN, di.TRACKING_NUMBER, " +
                            "case when di.WAREHOUSE in 'TRS1D' then 'DIRECT_SALES' when di.WAREHOUSE in 'TRCID' then 'WARRANTY' when di.WAREHOUSE in 'TRCDD' then " +
                            "'RETAIL' END WAREHOUSE from DBIT_RVURIMI.X_DS_DISPOSITIONS di,DBIT_RVURIMI.X_DS_SHIPMENTS sh where 1=1 and sh.X_ESN = di.SERIAL_NO " +
                            "and sh.BP_ORDER_NUM = '" + qParameter + "' and di.RCPT_DATE > sh.ORDER_DATE Union select di2.BP_ORDER, to_char(di2.RCPT_DATE,'MM/DD/YYYY') " +
                            "RCPT_DATE, D.RETURNED_ESN, di2.TRACKING_NUMBER,case when di2.WAREHOUSE in 'TRS1D' then 'DIRECT_SALES' when di2.WAREHOUSE in " +
                            "'TRCID' then 'WARRANTY' when di2.WAREHOUSE in 'TRCDD' then 'RETAIL' END WAREHOUSE from DBIT_RVURIMI.X_DS_DISPOSITIONS di2, " +
                            "(select pr.X_PART_SERIAL_NO RETURNED_ESN from sa.TABLE_CASE tc, sa.TABLE_X_PART_REQUEST pr,(select di.SERIAL_NO " +
                            "ESN_RETURNED,sh.SHIP_DATE from DBIT_RVURIMI.X_DS_DISPOSITIONS di,DBIT_RVURIMI.X_DS_SHIPMENTS sh where sh.X_ESN = di.SERIAL_NO and " +
                            "sh.BP_ORDER_NUM = '" + qParameter + "' and di.RCPT_DATE > sh.ORDER_DATE ) E where pr.REQUEST2CASE = tc.OBJID and pr.x_part_num_domain = 'PHONES' " +
                            "and tc.creation_time > E.SHIP_DATE  and (tc.X_ESN = E.ESN_RETURNED||'R' OR tc.X_ESN = E.ESN_RETURNED)) D where 1=1 and di2.SERIAL_NO = D.RETURNED_ESN";
                break;
            case "salesCases":
                theQuery = "Select tt.* from(select tc.X_ESN ORIGINAL_ESN, tc.ID_NUMBER CASE_ID, tc.TITLE CASE_TITLE, to_char(tc.CREATION_TIME,'MM/DD/YYYY') \n" +
                            "CASE_CREATION,pr.X_TRACKING_NO TRACKING_NO, to_char(pr.X_SHIP_DATE,'MM/DD/YYYY') SHIP_DATE,case when pr.X_PART_NUM_DOMAIN in 'ACC' then 'SHIPPING LABEL' \n" +
                            "when pr.X_PART_NUM_DOMAIN in 'PHONES' then 'HANDSET' END ITEM_SHIPPED, pr.X_PART_SERIAL_NO RETURN_TRACKING_OR_ESN from sa.TABLE_CASE tc, \n" +
                            "sa.TABLE_X_PART_REQUEST pr,(select distinct sh.X_ESN, sh.SHIP_DATE from DBIT_RVURIMI.X_DS_SHIPMENTS sh where sh.CLASS in 'DEVICES' and sh.BP_ORDER_NUM = \n" +
                            "'" + qParameter + "') M where pr.REQUEST2CASE = tc.OBJID and (tc.X_ESN = M.X_ESN or tc.X_ESN = M.X_ESN||'R') and tc.CREATION_TIME >= m.SHIP_DATE \n" +
                            "union select tc.X_ESN ORIGINAL_ESN, tc.ID_NUMBER CASE_ID, tc.TITLE CASE_TITLE, to_char(tc.CREATION_TIME,'MM/DD/YYYY') CASE_CREATION,'' TRACKING_NO, '' \n" +
                            "SHIP_DATE,'' ITEM_SHIPPED, '' RETURN_TRACKING_OR_ESN from sa.TABLE_CASE tc,(select distinct sh.X_ESN ESN, sh.SHIP_DATE from DBIT_RVURIMI.X_DS_SHIPMENTS \n" +
                            "sh where sh.CLASS in 'DEVICES' and sh.BP_ORDER_NUM IN ('" + qParameter + "')) M where 1 = 1 and (tc.X_ESN = M.ESN or tc.X_ESN = M.ESN||'R') and tc.CREATION_TIME \n" +
                            ">= m.SHIP_DATE and not exists (Select * from sa.TABLE_X_PART_REQUEST pr where pr.REQUEST2CASE = tc.OBJID) ) tt Order by tt.CASE_CREATION asc";
                break;
                
            default:
                throw new IllegalArgumentException("Invalid query selection");
        }
        return theQuery;
    }

    /**
     * Processes the client request by selecting and executing the specified
     * query There are 2 possible request.parameters: 1. qSelection: the name of
     * the query 2. qParameter: extra parameters to be added in the query This
     * method will request a connection to the DB and if successful it will
     * request the proper query and then execute it. It returns a string
     * containing the column names and values of the dataset
     *
     * @param request contains the http request from the client with parameters.
     * @param response contains the https response from the servlet. The method
     * assigns a writer to the response which will write the query result to
     * send to the client.
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, java.io.IOException
    {
        PrintWriter out = response.getWriter();
        Statement dbStmt = null;
        String qSelection = request.getParameter("qSelection");
        String qParameter = request.getParameter("qParameter");
        String db = request.getParameter("qDB");
        String dbCall = "";
        String finalResult = "";
        StringBuilder sb = new StringBuilder();
        Connection myConn = null;
        ResultSet dbResult = null;
        ResultSetMetaData dbMeta = null;
        int nColumns = 0;
        String result;

        try
        {
            myConn = (db.equals("undefined")) ? dbConnect() : dbConnect(db);
            if (myConn == null)
            {
                out.write("Connection to DB failed");
            } 
            else
            {
                dbStmt = myConn.createStatement();
                dbCall = getQuery(qSelection, qParameter);
                dbResult = dbStmt.executeQuery(dbCall);
                dbMeta = dbResult.getMetaData();
                nColumns = dbMeta.getColumnCount();
                for (int j = 1; j <= nColumns; j++)
                {
                    if (j > 1)
                        sb.append("*");
                    sb.append(dbMeta.getColumnName(j));
                }
                while (dbResult.next())
                {
                    sb.append("^");
                    for (int i = 1; i <= nColumns; i++)
                    {
                        if (i > 1)
                            sb.append("*");
                        result = dbResult.getString(i);
                        sb.append((result == null)? "" : result);
                    }
                }
                myConn.close();
            }
        } 
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        finalResult = sb.toString();
        out.write(finalResult);

        out.flush();
        out.close();
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String query = request.getParameter("query");
        String param = request.getParameter("param");

        PrintWriter out = response.getWriter();
        query = getQuery(query, param);
        try
        {
            out.write(query);
        }
        catch (Exception e)
        {
            out.println(e.getMessage());
        }
        finally
        {
            out.flush();
            out.close();
        }
    }
}
